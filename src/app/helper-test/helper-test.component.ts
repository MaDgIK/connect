import {Component, OnInit} from '@angular/core';
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {ActivatedRoute, Router} from "@angular/router";
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";


@Component({
  selector: 'theme',
  templateUrl: 'helper-test.component.html',
})

export class HelperTestComponent implements OnInit{

  public pageContents = null;
  properties:EnvProperties;

  constructor(private route: ActivatedRoute,
              private _router: Router,
              private helper: HelperService) {

  }

  ngOnInit(): void {
    this.route.data
      .subscribe((data: { envSpecific: EnvProperties }) => {
        this.properties = data.envSpecific;
        this.getPageContents();
      });
  }

  private getPageContents() {
    this.helper.getPageHelpContents(this.properties, "connect", this._router.url).subscribe(contents => {
      this.pageContents = contents;
    })
  }

}
