import { NgModule }            from '@angular/core';

import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {HelperTestComponent} from "./helper-test.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    HelperModule,
    CommonModule,
    RouterModule.forChild([
      {path: '', component: HelperTestComponent, canActivate: [IsRouteEnabled]}
    ])
  ],
  declarations: [
    HelperTestComponent
  ],
  providers:[IsRouteEnabled],
  exports: [
    HelperTestComponent
  ]
})


export class HelperTestModule{}
