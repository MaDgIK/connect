import {Component, OnDestroy, OnInit} from '@angular/core';
import {SearchCustomFilter} from "../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ActivatedRoute} from "@angular/router";
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";

@Component({
  selector: 'openaire-search-results',
  template: `
    <search-research-results resultType="result" [simpleView]="false" [customFilter]=customFilter
                             [hasPrefix]="false" [showBreadcrumb]="true"
                             [openaireLink]="'https://' + (properties.environment == 'production'?'':'beta.') + 'explore.openaire.eu/search/advanced/research-outcomes'"
                             [searchForm]="{dark: false, class: 'search-form'}"
    ></search-research-results>
  `
})
export class OpenaireSearchResearchResultsComponent implements  OnInit, OnDestroy {
  connectCommunityId: string;
  customFilter: SearchCustomFilter = null;
  properties: EnvProperties = properties;
  sub;
  
  constructor(private  route: ActivatedRoute, private _communityService: CommunityService) {
  }
  
  ngOnInit() {
      this.setCommunity();
  }
  
  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  
  setCommunity() {
    this.sub = this._communityService.getCommunityAsObservable().subscribe(community => {
      if (community != null) {
        this.connectCommunityId = community.communityId;
        this.customFilter = new SearchCustomFilter("Community", "communityId", this.connectCommunityId, community.shortTitle);
      }
    });
  }
}
