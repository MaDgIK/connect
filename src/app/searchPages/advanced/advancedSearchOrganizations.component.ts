import {Component, OnInit} from '@angular/core';
import {SearchCustomFilter} from "../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {ConnectHelper} from "../../openaireLibrary/connect/connectHelper";
import {properties} from "../../../environments/environment";


@Component({
    selector: 'openaire-advanced-search-organizations',
    template: `
    <search-organizations [simpleView]="false"   [customFilter]=customFilter [hasPrefix]="false" [showBreadcrumb]="true" [showSwitchSearchLink]="false" [searchForm]="{dark: false, class: 'search-form'}">
    </search-organizations>
    `
 })
export class OpenaireAdvancedSearchOrganizationsComponent implements OnInit {
  communityId:string;
  customFilter: SearchCustomFilter;
  
  constructor () {
  }
  
  ngOnInit() {
        let communityId =  ConnectHelper.getCommunityFromDomain(properties.domain);
        if(communityId){
          this.communityId = communityId;
          this.customFilter = new SearchCustomFilter("Community", "communityId", this.communityId, "");
          this.customFilter.isHiddenFilter = false;
        }
  }
}
