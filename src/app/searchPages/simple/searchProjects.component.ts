import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AdvancedField, Filter, Value} from '../../openaireLibrary/searchPages/searchUtils/searchHelperClasses.class';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {ErrorMessagesComponent} from '../../openaireLibrary/utils/errorMessages.component';
import {SearchFields} from '../../openaireLibrary/utils/properties/searchFields';
import {SearchCustomFilter, SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {SearchCommunityProjectsService} from '../../openaireLibrary/connect/projects/searchProjects.service';
import {properties} from "../../../environments/environment";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";
import {Subscriber} from "rxjs";
import {NewSearchPageComponent} from "../../openaireLibrary/searchPages/searchUtils/newSearchPage.component";
import {SearchResult} from "../../openaireLibrary/utils/entities/searchResult";
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {IndexInfoService} from "../../openaireLibrary/utils/indexInfo.service";

@Component({
    selector: 'openaire-search-projects',
    template: `
 <new-search-page
   pageTitle="Search Projects"
   entityType="project"
   type="project"
   [results]="results"
   [searchUtils]="searchUtils"
   [sortedByChanged]="searchUtils.sortBy"
   [fieldIds]="fieldIds" [fieldIdsMap]="fieldIdsMap" [selectedFields]="selectedFields"

   [simpleSearchLink]="properties.searchLinkToProjects"
   [disableForms]="disableForms"
   [disableRefineForms]="disableRefineForms"
   [loadPaging]="loadPaging"
   [oldTotalResults]="oldTotalResults"
   [openaireLink]="'https://' + (properties.environment == 'production'?'':'beta.') + 'explore.openaire.eu/search/find/projects'"
   [includeOnlyResultsAndFilter]="false"
   [hasPrefix]="false"
   searchFormClass="datasourcesTableSearchForm" [entitiesSelection]="false" [showSwitchSearchLink]="false"
   [filters]="filters"
   [simpleView]="true" formPlaceholderText="Search by name..."
   [showResultCount]="false" [showIndexInfo]="false" [showDownload]="false"
   [sort]="false" [showBreadcrumb]="true"
   [customFilter]=customFilter [searchForm]="{dark: false, class: 'search-form'}">
 </new-search-page>
    `

})
export class OpenaireSearchProjectsComponent {
  private errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;
  public columnNames = ['Project', 'GrantId', 'Funder'];
  public results =[];
  public originalFunders =[];
  public filters =[];
  selectedFunder = null;
  public searchUtils:SearchUtilsClass = new SearchUtilsClass();
  subscriptions = [];
  public searchFields:SearchFields = new SearchFields();
  public refineFields: string[] = ["funder"];
  properties:EnvProperties = properties;

  public disableForms: boolean = false;
  public enableSearchView: boolean = true;

  private communityId: string = '';
  customFilter: SearchCustomFilter = null;
  initialLoad = true;
  @ViewChild(NewSearchPageComponent, { static: true }) searchPage: NewSearchPageComponent;
  public fieldIds:  string[] = this.searchFields.PROJECT_ADVANCED_FIELDS;
  public fieldIdsMap= this.searchFields.PROJECT_FIELDS;
  public selectedFields:AdvancedField[] =  [];
  public disableRefineForms: boolean = false;
  public loadPaging: boolean = true;
  public oldTotalResults: number = 0;
  keyword;
  lastDBLoadDate = null;
  constructor (private route: ActivatedRoute, private _searchProjectsService: SearchCommunityProjectsService,  private _communityService: CommunityService, private indexInfoService: IndexInfoService) {
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.searchUtils.status = this.errorCodes.LOADING;
  }

  public ngOnInit() {
    this.subscriptions.push(this.indexInfoService.getLastIndexDate(this.properties).subscribe(res => {
      this.lastDBLoadDate = res;
    }));
    this.searchUtils.baseUrl = this.properties.searchLinkToProjects;
     this.subscriptions.push(this._communityService.getCommunityAsObservable().subscribe(community =>{
        if(community != null){
          this.communityId = community.communityId;
          this.customFilter = new SearchCustomFilter("Community", "communityId", this.communityId, community.shortTitle);
          this.subscriptions.push(this.route.queryParams.subscribe(params => {
            let page = (params['page'] === undefined) ? 1 : +params['page'];
            this.searchUtils.page = (page <= 0) ? 1 : page;
            this.selectedFunder = params["funder"]?StringUtils.unquote(params["funder"]):null;
            this.searchUtils.validateSize(params['size']);
            this.keyword = decodeURIComponent(params['fv0']?params['fv0']:(params['keyword']?params['keyword']:''));
            this.selectedFields = [];
            this.searchPage.prepareSearchPage(this.fieldIds, this.selectedFields, this.refineFields, [], ["funder"], this.fieldIdsMap, this.customFilter, params, "project");
            if(this.initialLoad) {
              this.initialLoad = false;
              this._getCommunityFunders();
            }else{
              this._getResults();
            }
          }));
        }
    }));
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
    private _getResults(){
      this.results = [];
      this.searchUtils.totalResults = 0;
      this.searchUtils.refineStatus = this.errorCodes.LOADING;
      this.searchUtils.status = this.errorCodes.LOADING;
      this.disableForms = true;
      this.disableRefineForms = true;
      this.enableSearchView = false;
      this.subscriptions.push(this._searchProjectsService.searchProjectsWithPaging(this.properties, this.communityId, this.searchUtils.page, this.searchUtils.size, this.keyword, this.selectedFunder, "name" ).subscribe(
        data => {
          this.searchUtils.totalResults = data['totalElements'];
          this.filters = this.selectedFunder?this.createFilters([this.selectedFunder]):this.createFilters(this.originalFunders);
          this.results = this.parseResults(data.content);
          this.oldTotalResults = data['totalElements'];
          this.searchUtils.totalResults = data['totalElements'];
          this.searchUtils.status = this.results.length == 0 ? this.errorCodes.NONE:  this.errorCodes.DONE;
          this.disableForms = false;
          this.disableRefineForms = false;
          this.enableSearchView = true;
          this.searchUtils.refineStatus = this.errorCodes.DONE;
          this.searchPage.prepareFiltersToShow(this.filters, this.searchUtils.totalResults);
        },
        err => {
          this.handleError("Error getting projects for community with id: " + this.communityId, err);
          this.searchUtils.status = this.errorMessages.getErrorCode(err.status);
          this.enableSearchView = true;
        }
      ));

    }


  private createFilters(data):Filter[] {
    var funders = new Set<String>();
    var value_name = [];
    var value_original_id = [];
    for(let funder of data) {
      funders.add(funder);
      value_name.push(funder);
      value_original_id.push(funder);
    }


    var filters: Filter[] =[];
      if(value_name.length > 0) {
        var values: Value[] = [];
        for (var j = 0; j < value_name.length; j++) {
          var value: Value = {name: value_name[j], id:value_name[j], number: 0, selected: this.selectedFunder ==  value_name[j] }
          values.push(value);
        }
        var filter: Filter = {
          title: "Funder",
          filterId: "funder",
          originalFilterId: "funder",
          values: values,
          countSelectedValues: this.selectedFunder?1:0,
          "filterOperator": 'or',
          valueIsExact: true,
          filterType: "checkbox"
        };
        filters.push(filter);
      }

    return filters;
  }

  parseResults(data){
    let results:SearchResult[]=[];
    for(let result of data){
      let sResult:SearchResult = new SearchResult();
      if(!result["availableSince"] ||  !this.lastDBLoadDate || (result["availableSince"] && this.lastDBLoadDate && result["availableSince"] < this.lastDBLoadDate)){
        sResult.id = result.openaireId;
      }else{
        sResult.id = "-1"; //not yet in the graph
      }
      sResult.title = {name:"", accessMode: null};
      sResult.title.name = result.name?result.name:result.acronym;
      sResult.acronym = result['acronym'];
      sResult.code = result['grantId'];
      sResult['funderShortname'] = result['funder'];
      results.push(sResult);
    }
    return results;
  }
  getFunderFilterValue(){
    for(let filter of this.filters){
      if(filter.countSelectedValues > 0){
        for(let value of filter.values){
          if(value.selected) {
           return value.name;
          }
        }
      }
    }
   return null;

  }
  private handleError(message: string, error) {
    console.error("Projects simple Search Page: "+message, error);
  }

  public _getCommunityFunders() {
    this.subscriptions.push(this._searchProjectsService.getProjectFunders(this.properties, this.communityId).subscribe(
      data => {
        this.originalFunders = data
        this._getResults();
      },
      err => {
      }
    ));
  }

}
