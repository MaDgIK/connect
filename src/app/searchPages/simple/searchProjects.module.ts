import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {SearchProjectsRoutingModule} from './searchProjects-routing.module';
import {OpenaireSearchProjectsComponent} from './searchProjects.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {DataProvidersServiceModule} from '../../openaireLibrary/services/dataProvidersService.module';
import {SearchProjectsServiceModule} from '../../openaireLibrary/connect/projects/searchProjectsService.module';
import {NewSearchPageModule} from "../../openaireLibrary/searchPages/searchUtils/newSearchPage.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, SearchProjectsRoutingModule,
    DataProvidersServiceModule, SearchProjectsServiceModule, NewSearchPageModule

  ],
  declarations: [
    OpenaireSearchProjectsComponent
   ],
  providers:[PreviousRouteRecorder],
  exports: [
    OpenaireSearchProjectsComponent
     ]
})
export class LibSearchProjectsModule { }
