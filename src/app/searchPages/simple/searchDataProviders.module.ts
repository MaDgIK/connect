import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {OpenaireSearchDataprovidersComponent} from './searchDataproviders.component';
import {SearchDataProvidersRoutingModule} from './searchDataProviders-routing.module';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchDataprovidersServiceModule} from '../../openaireLibrary/connect/contentProviders/searchDataprovidersService.module';
import {NewSearchPageModule} from "../../openaireLibrary/searchPages/searchUtils/newSearchPage.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchDataProvidersRoutingModule, SearchDataprovidersServiceModule, NewSearchPageModule

  ],
  declarations: [
    OpenaireSearchDataprovidersComponent
  ],
  providers: [PreviousRouteRecorder],
  exports: [
    OpenaireSearchDataprovidersComponent
  ]
})
export class LibSearchDataProvidersModule {}
