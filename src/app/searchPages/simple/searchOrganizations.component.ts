import {Component} from '@angular/core';

@Component({
  selector: 'openaire-search-organizations',
  template: `
    <search-organizations [searchForm]="{dark: false, class: 'search-form'}"></search-organizations>
  `
})
export class OpenaireSearchOrganizationsComponent{
}
