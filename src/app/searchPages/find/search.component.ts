import {Component} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {SearchCustomFilter} from "../../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {properties} from "../../../environments/environment";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";

@Component({
  selector: 'openaire-search-find',
  template: `
    <search-all logoURL="/assets/common-assets/logo-small-explore.png" name="OpenAIRE Connect"
                [customFilter]=customFilter></search-all>
  `,
})
export class OpenaireSearchComponent {
  communityId: string;
  customFilter: SearchCustomFilter = null;
  properties: EnvProperties = properties;
  
  constructor() {
  }
  
  ngOnInit() {
    let communityId = ConnectHelper.getCommunityFromDomain(this.properties.domain);
    if (communityId) {
      this.communityId = communityId;
      this.customFilter = new SearchCustomFilter("Community", "communityId", this.communityId, "");
    }
  }
}
