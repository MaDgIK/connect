import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SearchCommunitiesComponent} from "./searchCommunities.component";
import {SearchFormModule} from "../../openaireLibrary/searchPages/searchUtils/searchForm.module";
import {SearchCommunitiesRoutingModule} from "./searchCommunities-routing.module";
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {NewSearchPageModule} from "../../openaireLibrary/searchPages/searchUtils/newSearchPage.module";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchFormModule, SearchCommunitiesRoutingModule, NewSearchPageModule
  ],
  declarations: [
    SearchCommunitiesComponent
  ],
  providers: [
    PreviousRouteRecorder,
    IsRouteEnabled
  ],
  exports: [
    SearchCommunitiesComponent
  ]
})
export class SearchCommunitiesModule {
}
