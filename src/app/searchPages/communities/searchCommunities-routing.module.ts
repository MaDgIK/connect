import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SearchCommunitiesComponent} from "./searchCommunities.component";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: SearchCommunitiesComponent, canActivate: [IsRouteEnabled],canDeactivate: [PreviousRouteRecorder] }
    ])
  ]
})
export class SearchCommunitiesRoutingModule { }
