import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-directLinking',
  template: `
    <directLinking [communityId]=communityId></directLinking>`
})
export class OpenaireDirectLinkingComponent {
  communityId: string;
  sub;

  constructor(private route: ActivatedRoute) {}

  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }
  public ngOnInit() {
    this.sub = this.route.queryParams.subscribe(
          communityId => {
            this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
            if (!this.communityId) {
              this.communityId = communityId['communityId'];
            }
          });
  }
}
