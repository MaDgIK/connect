import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {OpenaireLinkingComponent} from './linkingGeneric.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../openaireLibrary/error/isRouteEnabled.guard';
import {ConnectSubscriberGuard} from '../../openaireLibrary/connect/communityGuard/connectSubscriber.guard';
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireLinkingComponent, canActivate: [IsRouteEnabled, LoginGuard, ConnectSubscriberGuard ], data: {
          redirect: '/error'
        }, canDeactivate: [PreviousRouteRecorder]},

    ])
  ]
})
export class LinkingRoutingModule { }
