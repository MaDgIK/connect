import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";

@Component({
    selector: 'openaire-linking-generic',
    template: `<linking-generic [communityId]=communityId pageTitle="Linking"></linking-generic>`
})
export class OpenaireLinkingComponent {
  communityId:string;
  sub;

  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }

  constructor (private route: ActivatedRoute) {
      this.route.queryParams.subscribe(
        communityId => {
          this.communityId  = ConnectHelper.getCommunityFromDomain(properties.domain);
          if(!this.communityId) {
            this.communityId = communityId['communityId'];
          }
        });
  }
}
