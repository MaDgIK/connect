import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConnectHelper} from "../../openaireLibrary/connect/connectHelper";
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";


@Component({
    selector: 'openaire-my-claims',
    template: `
    <my-claims  [claimsInfoURL]="claimsInfoURL" [communityId]=communityId></my-claims>
`

})
 export class OpenaireMyClaimsComponent {
  claimsInfoURL:string;
  communityId:string;
  sub;

  constructor (private route: ActivatedRoute) {}

  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }

  public ngOnInit() {
    this.claimsInfoURL = properties.claimsInformationLink;
     this.sub = this.route.queryParams.subscribe(
       communityId => {
         this.communityId  = ConnectHelper.getCommunityFromDomain(properties.domain);
         if(!this.communityId) {
           this.communityId = communityId['communityId'];
         }
       });
  }
}
