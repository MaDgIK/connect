import {Component} from '@angular/core';
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'projects',
  template: `
    <div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div class="uk-padding-small uk-padding-remove-horizontal">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
      <html-page description="Projects and funding Opportunities" pageTitle="Projects and funding Opportunities"></html-page>
    </div>
  `
})
export class ProjectsPageComponent {
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'about - Projects and funding Opportunities'}];
}
