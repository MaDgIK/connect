import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {ProjectsPageComponent} from './projectsPage.component';
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages.module";
import {ProjectsPageRoutingModule} from "./projects-routing.module";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";


@NgModule({
  imports: [
    ProjectsPageRoutingModule, RouterModule, CommonModule, HtmlPagesModule, BreadcrumbsModule
  ],
  declarations: [
    ProjectsPageComponent
  ],
  providers: [ PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    ProjectsPageComponent
  ]
})


export class ProjectsPageModule {
}
