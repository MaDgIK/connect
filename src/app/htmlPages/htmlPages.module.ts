import {NgModule} from '@angular/core';
import {HtmlPageComponent} from './htmlPage.component';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {PiwikServiceModule} from "../openaireLibrary/utils/piwik/piwikService.module";

@NgModule({
  imports: [
    RouterModule, CommonModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, PiwikServiceModule
  ],
  declarations: [
    HtmlPageComponent
  ],
  exports: [
    HtmlPageComponent
  ]
})


export class HtmlPagesModule{}
