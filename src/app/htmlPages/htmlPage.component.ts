import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {Subscriber} from "rxjs";
import {properties} from "../../environments/environment";
import {ConfigurationService} from "../openaireLibrary/utils/configuration/configuration.service";

@Component({
  selector: 'html-page',
  template: `
    <schema2jsonld *ngIf="url" [URL]="url" [name]="pageTitle" type="other" [description]="description"></schema2jsonld>
    <helper *ngIf="pageContents && pageContents['top'] && pageContents['top'].length > 0"
            [texts]="pageContents['top']"></helper>
  `
})
export class HtmlPageComponent {
  properties: EnvProperties = properties;
  public pageContents = null;
  public divContents = null;

  @Input() url: string = null;
  @Input() pageTitle: string;
  @Input() description: string;
  private subscriptions = [];
  communityId;

  constructor(private route: ActivatedRoute, private _router: Router,
              private _meta: Meta,
              private _title: Title,
              private seoService: SEOService,
              private _piwikService: PiwikService,
              private configurationService: ConfigurationService,
              private helper: HelperService) {
  }

  public ngOnInit() {
    this.subscriptions.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
    this.communityId = ConnectHelper.getCommunityFromDomain(this.properties.domain);
    this.url = this.properties.domain + this.properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(this.url);
    this.updateUrl(this.url);
    this.updateTitle(this.pageTitle);
    this.updateDescription(this.description);
    this.getPageContents();
    
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
  
  private getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, this.communityId ? this.communityId : "connect", this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
}
