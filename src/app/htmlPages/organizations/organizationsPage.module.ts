import { NgModule }            from '@angular/core';

import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../openaireLibrary/error/isRouteEnabled.guard'

import {OrganizationsPageComponent} from './organizationsPage.component';
import {OrganizationsPageRoutingModule} from './organizationsPage-routing.module';

import {AffiliationsModule} from "../../openaireLibrary/connect/affiliations/affiliations.module";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages.module";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  imports: [
    OrganizationsPageRoutingModule, AffiliationsModule, HelperModule, CommonModule, HtmlPagesModule,
    BreadcrumbsModule
  ],
  declarations: [
    OrganizationsPageComponent
  ],
  providers:[PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    OrganizationsPageComponent
  ]
})


export class OrganizationsPageModule{}
