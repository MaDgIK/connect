import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {OrganizationsPageComponent} from './organizationsPage.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../openaireLibrary/error/isRouteEnabled.guard'

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: OrganizationsPageComponent, canActivate: [ IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class OrganizationsPageRoutingModule { }
