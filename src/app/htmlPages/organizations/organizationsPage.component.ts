import {Component} from '@angular/core';
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
    selector: 'organizations',
    template: `
			<div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
				<div class="uk-padding-small uk-padding-remove-horizontal">
					<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
				</div>
			</div>
			<div class="uk-container uk-container-large uk-section uk-section-small uk-margin-medium-bottom">
				<h1 class="uk-margin-top uk-margin-large-bottom uk-width-1-2@m">Supporting Organizations</h1>
				<html-page description="OpenAIRE - Connect, Community Gateway, research community, organizations" pageTitle="Supporting Organizations"></html-page>
				<affiliations [longView]="true" [getAffiliationsFromAPI]="true"></affiliations>
			</div>
    `
})
export class OrganizationsPageComponent {
  public breadcrumbs: Breadcrumb[] = [{name: 'Home', route: '/'}, {name: 'About - Supporting Organizations'}];

  constructor () {}
  public ngOnInit() {
  }

}
