import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {PublicationsPageComponent} from './publications-page.component';
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages.module";
import {PublicationsPageRoutingModule} from "./publications-routing.module";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";


@NgModule({
  imports: [
    PublicationsPageRoutingModule, RouterModule, CommonModule,   HtmlPagesModule
  ],
  declarations: [
    PublicationsPageComponent
  ],
  providers: [ PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    PublicationsPageComponent
  ]
})


export class PublicationsPageModule {
}
