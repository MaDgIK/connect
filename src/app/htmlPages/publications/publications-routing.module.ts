import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {PublicationsPageComponent} from './publications-page.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: PublicationsPageComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class PublicationsPageRoutingModule { }
