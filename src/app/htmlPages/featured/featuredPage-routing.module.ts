import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FeaturedPageComponent} from './featuredPage.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: FeaturedPageComponent,
        canActivate: [IsRouteEnabled],
        canDeactivate: [PreviousRouteRecorder]
      }

    ])
  ]
})
export class FeaturedPageRoutingModule {
}
