import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {ConfigurationService} from "../../openaireLibrary/utils/configuration/configuration.service";
import {Portal} from "../../openaireLibrary/utils/entities/adminTool/portal";
import {Page} from "../../openaireLibrary/utils/entities/adminTool/page";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'featured',
  template: `
    <div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div *ngIf="breadcrumbs" class="uk-padding-small uk-padding-remove-horizontal">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
      <html-page [description]="page ? page.name : 'Featured'" [pageTitle]="page ? page.name : 'Featured'"></html-page>
    </div>
  `
})
export class FeaturedPageComponent {
  public breadcrumbs: Breadcrumb[];
  public page: Page;
  subs: Subscription[] = [];

  constructor(private config: ConfigurationService, private _router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.subs.push(this.route.data.subscribe(data => {
      if(data?.breadcrumbs) {
        this.breadcrumbs = data.breadcrumbs;
      }
    }))
    this.subs.push(this.config.portalAsObservable.subscribe((portal: Portal) => {
      if (portal) {
        let pages: Page[] = <Page[]>portal.pages;
        this.page = pages.find(page => (page.route == this._router.url));
      }
    }));
  }

  ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }
}
