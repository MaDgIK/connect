import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {FeaturedPageComponent} from './featuredPage.component';
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages.module";
import {FeaturedPageRoutingModule} from "./featuredPage-routing.module";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";


@NgModule({
  imports: [
    FeaturedPageRoutingModule, RouterModule, CommonModule, HtmlPagesModule, BreadcrumbsModule
  ],
  declarations: [
    FeaturedPageComponent
  ],
  providers: [PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    FeaturedPageComponent
  ]
})


export class FeaturedPageModule {
}
