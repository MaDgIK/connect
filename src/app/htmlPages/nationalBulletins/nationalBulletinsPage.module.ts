import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {NationalBulletinPageComponent} from './nationalBulletinsPage.component';
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages.module";
import {NaionalBulletinPageRoutingModule} from "./nationalBulletinsPage-routing.module";
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";


@NgModule({
  imports: [
    NaionalBulletinPageRoutingModule, RouterModule, CommonModule, HtmlPagesModule, BreadcrumbsModule
  ],
  declarations: [
    NationalBulletinPageComponent
  ],
  providers: [PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    NationalBulletinPageComponent
  ]
})


export class NaionalBulletinPageModule {
}
