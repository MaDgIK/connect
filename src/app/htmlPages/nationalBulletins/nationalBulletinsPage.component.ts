import {Component} from '@angular/core';
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'national-bulletin',
  template: `
    <div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div class="uk-padding-small uk-padding-remove-horizontal">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
      <html-page description="National Bulletins" pageTitle="National Bulletins"></html-page>
    </div>
  `
})
export class NationalBulletinPageComponent {
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'about - National Bulletins'}];
}
