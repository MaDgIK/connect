import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PricingComponent} from "./pricing.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({

  imports: [CommonModule, RouterModule, RouterModule.forChild([
    {
      path: '',
      component: PricingComponent,
      canDeactivate: [PreviousRouteRecorder],
      canActivate: [IsRouteEnabled]
    },
  ]), PageContentModule, IconsModule, BreadcrumbsModule],
  declarations: [PricingComponent],
  exports: [PricingComponent]
})
export class PricingModule {

}
