import {Component, OnDestroy, OnInit} from "@angular/core";
import {CommunityService} from "../openaireLibrary/connect/community/community.service";
import {Subscription} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {properties} from "../../environments/environment";
import {Router} from "@angular/router";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'pricing',
  template: `
    <div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div class="uk-padding-small">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
    </div>
    <div class="uk-container uk-container-large">
      <div class="">
        <h1>Subscriptions</h1>

        <div>
          <div>
            As demand for our services grows, along with the features included in each of them, traditional sources of
            funding may not be sufficient. By introducing subscription fees, we can ensure their long-term
            sustainability; by achieving financial stability we can continue to invest in better resources, technology,
            and talent, thereby increasing our capacity to deliver impactful programs and services to all interested
            parties. Subscriptions support immediate operational needs, while at the same time enabling us to scale our
            efforts and make a greater, more sustainable difference in the communities we serve.
          </div>
        </div>
      </div>
      <div class="uk-section">

        <table id="pricing-table" class="uk-table uk-table-striped uk-box-shadow-medium uk-text-center">
          <tbody>
          <tr>
            <td></td>
            <td>
              <p class="uk-text-primary"><strong>STANDARD</strong></p>
            </td>
            <td class="">
              <p class="uk-text-primary"><strong>ADVANCED</strong></p>
            </td>
            <td>
              <p class="uk-text-primary"><strong>PREMIUM</strong></p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Suggested for</strong></p>
            </td>
            <td>
              <p>Thematic Communities</p>
            </td>
            <td>
              <p>Research Infrastructures or Communities</p>
            </td>
            <td>
              <p>Research Organisations and Networks</p>
            </td>
          </tr>

          <!-- <tr>
             <td colspan="4">
               <p><strong>Features</strong></p>
             </td>
           </tr>-->
          <tr>
            <td>
              <p class="uk-text-left"><strong>Gateway on the cloud</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Gateway Customisation for Menu and Page Editing, and your Brand</strong>
              </p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Customised pages</strong></p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Unlimited Number of Managers/Administrators</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Login via OpenAIRE AAI</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Standard search &amp; browse </strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Search &amp; browse by sub-communities</strong></p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✘</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Search &amp; browse by organisation members</strong></p>
            </td>
            <td>
              <p>✘</p>

            </td>
            <td>
              <p>✘</p>

            </td>
            <td>
              <p>coming soon</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Fields of Science classification</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Sustainable Development Goals classification</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Research Linking Functionality</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Link with your Zenodo Community</strong></p>

            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Curated Affiliations by OpenAIRE via OpenOrgs</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Text mining</strong></p>

            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Customised text mining</strong></p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Default Charts</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Customised charts per organisation</strong></p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>coming soon</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Data Update (via OpenAIRE Graph)</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Integration of Community Services*</strong>
                <br>
                <span class="uk-text-small">*<i>subject to feasibility study</i></span>
              </p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Metadata Access via APIs</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Metadata Snapshots Every 6 Months if Public</strong></p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>

            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Frequent metadata snapshot upon request</strong></p>
            </td>
            <td>
              <p>✘</p>
            </td>
            <td>
              <p>✔</p>
            </td>
            <td>
              <p>✔</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Support time to reply</strong></p>
            </td>
            <td>
              <p>72h</p>
            </td>
            <td>
              <p>48h</p>
            </td>
            <td>
              <p>48h</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Training</strong></p>
            </td>
            <td>
              <p>1h</p>
            </td>
            <td>
              <p>4h</p>
            </td>
            <td>
              <p>10h</p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Backup, archive after the subscription ends</strong></p>
            </td>
            <td>
              <p>Up to 1 month</p>

            </td>
            <td>
              <p>Up to 2 months</p>

            </td>
            <td>
              <p>Up to 2 months</p>

            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong><span>Trial Period
                <!-- <span uk-icon="info" uk-tooltip="Try OpenAIRE CONNECT for free"></span>--></span></strong></p>
            </td>
            <td>
              <p><span>2 months</span></p>
            </td>
            <td>
              <p><span>3 months</span></p>
            </td>
            <td>
              <p><span>3 months</span></p>
            </td>
          </tr>
          <tr>
            <td>
              <p class="uk-text-left"><strong>Annual fee (€)</strong></p>
            </td>
            <td>
              <p>4000</p>
            </td>
            <td>
              <p>5500</p>
            </td>
            <td>
              <p>5500 + <i>800 per member</i></p>
            </td>
          </tr>
          </tbody>
        </table>
        <div>
          <ul>
            <!--<li><b> CONNECT+MONITOR BUNDLE</b>: It’s possible to combine OpenAIRE CONNECT Advanced and OpenAIRE
              Monitor
              premium at the Special Price of 10 000€
            </li>-->
            <!--            <li> OpenAIRE members get a 30% discount</li>-->
            <li>0% VAT (24% VAT applies only for Greek TAX ID number holders)</li>
          </ul>
        </div>
      </div>
      <div class="uk-section">
        <h2>Special Packages</h2>

        <div class="uk-margin-large-top">
          <div class="uk-child-width-1-2@m uk-child-width-1-1@s uk-grid-large uk-grid-match uk-grid" uk-grid=""
               uk-height-match=".target">
            <div class="uk-first-column">
              <div class="uk-card uk-card-default uk-padding">
                <!--<div class="uk-card-media-top uk-flex uk-flex-middle uk-flex-center
\t\t\t\t\t\t\t\tuk-padding-small uk-padding-remove-vertical uk-margin-top uk-margin-bottom">
                    <div>
                        <img src="assets/connect-assets/home/virtual.svg"
                             alt="A Virtual Research Environment" style="height:60px; width:67px"/>
                    </div>
                </div>-->
                <div class="uk-card-body ">
                  <div class="target uk-text-center">
                    <h5 class=" uk-card-title uk-margin-medium-bottom">OpenAIRE CONNECT and MONITOR <br>at the special price
                      of 10 000€</h5>
                    <!--<div>
                      It’s possible to combine OpenAIRE CONNECT Advanced and OpenAIRE
                      MONITOR
                      Premium at the special price of 10 000€

                    </div>-->
                  </div>

                </div>
              </div>
            </div>


            <div class="uk-first-column">
              <div class="uk-card uk-card-default uk-padding">
                <!--<div class="uk-card-media-top uk-flex uk-flex-middle uk-flex-center
\t\t\t\t\t\t\t\tuk-padding-small uk-padding-remove-vertical uk-margin-top uk-margin-bottom">
                    <div>
                        <img src="assets/connect-assets/home/virtual.svg"
                             alt="A Virtual Research Environment" style="height:60px; width:67px"/>
                    </div>
                </div>-->
                <div class="uk-card-body ">
                  <div class="target uk-text-center">
                    <h5 class=" uk-card-title uk-margin-medium-bottom">OpenAIRE members <br>get a 30% discount</h5>
                    <!--<div>
                      OpenAIRE members get a 30% discount
                    </div>-->
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="uk-container uk-container-large">
      <div class="uk-section">
        <h2>Description of the features </h2>
        <div>The OpenAIRE CONNECT Gateway provides a comprehensive set of features designed to support and enhance research communities in bringing forward their results and Open Science activities/practices.
        </div>


        <table class="uk-table uk-table-striped uk-table-responsive uk-box-shadow-medium uk-margin-large-top">
          <tbody>
          <tr style="cursor: pointer" uk-toggle="target: .toggle">
            <td class="uk-width-medium@m">
              <p class="uk-text-left"><strong>Features</strong></p>
            </td>
            <td class="">
              <p class="uk-text-left"><strong>Description</strong></p>
            </td>
            <td style="width:30px">
              <icon name="expand_more" class=" toggle"></icon>
              <icon class=" toggle" hidden name="expand_less"></icon>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Gateway on the Cloud</strong></p>
            </td>
            <td>
              <p>Full IT support of the service: hosting, installation, maintenance, upgrade, backups. Ensures
                reliable
                and secure cloud deployment on the OpenAIRE infrastructure, providing scalable and accessible
                cloud
                services.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Gateway Customisation for Menu and Page Editing, and your
                Brand</strong></p>
            </td>
            <td>
              <p>Create your own menu, edit pages, and adapt to your visual identity with logo, banner,
                background,
                buttons, colours. Edit the home page to include your personalised content.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Customised pages</strong></p>
            </td>
            <td>
              <p>Request new pages for you to customise.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Unlimited Number of Managers/Administrators</strong></p>
            </td>
            <td>
              <p>Supports an unlimited number of administrators or managers, enhancing collaborative management
                and
                oversight capabilities.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Login via OpenAIRE AAI</strong></p>
            </td>
            <td>
              <p>Sign in via an institutional account (eduGAIN), GitHub, Google, LinkedIn, ORCID, or with an
                OpenAIRE
                account.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Standard Search &amp; Browse</strong></p>
            </td>
            <td>
              <p>Search and browse for research products, projects, and data sources.</p>
              <p>Users can use all the filters of <a href="https://explore.openaire.eu/" target="_blank">OpenAIRE
                EXPLORE,</a>
                including
                filters for type of products (publications, data, software and more), FoS (Field of Science) and
                SDGs
                (UN Sustainable Development Goals). You can enable or disable the search functionality on specific
                entity types according to your needs.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Search &amp; browse by sub-communities</strong></p>
            </td>
            <td>
              <p>You define how your community is organised (e.g. by theme, by geographical nodes). OpenAIRE
                CONNECT
                can
                associate research products to the sub-communities and offer dedicated options to search and
                browse.</p>
              <p>Available for CONNECT Advanced.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Search &amp; browse by organisation members</strong></p>
            </td>
            <td>
              <p>Search and browse for organisations that are members of your research communities. Filter
                research
                products by organisation.</p>
              <p>Coming soon for CONNECT Premium.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Fields of Science classification</strong></p>
            </td>
            <td>
              <p>Publications are classified by SciNoBo: a novel system classifying scholarly communication in a
                dynamically constructed hierarchical Field-of-Science taxonomy -
                <a href="https://doi.org/10.3389/frma.2023.1149834" target="_blank">https://doi.org/10.3389/frma.2023.1149834</a>
              </p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Sustainable Development Goals classification</strong></p>
            </td>
            <td>
              <p>Publications are classified by SciNoBo. For more information, please visit
                <a href="https://www.openaire.eu/openaire-explore-introducing-sdgs-and-fos" target="_blank">https://www.openaire.eu/openaire-explore-introducing-sdgs-and-fos</a>
              </p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Research Linking Functionality</strong></p>
            </td>
            <td>
              <p>Allows your users to link articles, dataset, or software within a same project (see <a
                  href="https://www.openaire.eu/claim-publication" target="_blank">this guide</a>) and claim their
                authorship with
                ORCID
                (<a href="https://www.openaire.eu/openaire-explore-integration-with-the-orcid-search-and-link-wizard"
                    target="_blank">guide</a>)
              </p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Link with your Zenodo Community</strong></p>
            </td>
            <td>
              <p>Seamlessly integrates with Zenodo communities, allowing users to manage and share their research
                outputs within a dedicated community space.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Curated Affiliations by OpenAIRE Team via OpenOrgs</strong></p>
            </td>
            <td>
              <p>Utilizes affiliations curated by the OpenAIRE team, ensuring data accuracy and organisational
                integrity
                through verified affiliations.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Text mining</strong></p>
            </td>
            <td>
              <p>Employs advanced data mining algorithms to extract and analyse data, enhancing the platform's
                ability
                to provide deep insights and comprehensive research intelligence. Learn more about the text mining
                algorithms on the <a href="https://graph.openaire.eu/what-is-the-openaire-graph#Id-How"
                                     target="_blank">web site of
                  the
                  OpenAIRE Graph</a>.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Customised text mining</strong></p>
            </td>
            <td>
              <p>OpenAIRE develops a text mining algorithm for the identification of research products of your
                community. You help us with the tests and fine tuning to reach the optimal precision and
                recall.</p>
              <p>Available for CONNECT Advanced and Premium.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Default Charts</strong></p>
            </td>
            <td>
              <p>A set of charts to check the uptake of Open Science practices within your community: percentage
                of
                Open
                Access publications and datasets, geographic distribution of publications, datasets with
                persistent
                Identifiers over time, download counts of publications by year, and publications by access rights
                over
                time.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Customised charts per organisation</strong></p>
            </td>
            <td>
              <p>The set of default charts customised for the organisations that are members of your research
                community.</p>
              <p>Coming soon for CONNECT Premium.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Data Update (via OpenAIRE Graph)</strong></p>
            </td>
            <td>
              <p>Regularly, every time the OpenAIRE Graph is updated (about once a month).</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Integration of community services</strong></p>
            </td>
            <td>
              <p>Integration of services developed and operated by your research community into your gateway. The
                integration is subject to a feasibility study.</p>
              <p>Available for CONNECT Advanced and Premium.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Metadata Access via APIs</strong></p>
            </td>
            <td>
              <p>Provides API access to metadata, allowing for integration with other systems and enabling
                developers
                to
                build custom applications or enhance existing systems.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Metadata Snapshots Every 6 Months if Public</strong></p>
            </td>
            <td>
              <p>Takes snapshots of metadata every six months for publicly available data, facilitating data
                preservation and historical analysis.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Support time to reply</strong></p>
            </td>
            <td>
              <p>We support you during the business hours of the week, generally defined as 9:00 - 17:00 CET
                excluding
                holidays and weekends. The OpenAIRE team is an international team working in Europe in different
                time
                zones.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Training</strong></p>
            </td>
            <td>
              <p>Online training sessions to help you set up your gateway. Based on your needs we can organise
                trainings
                on other OpenAIRE services related to CONNECT like PROVIDE (to support your repositories at
                becoming
                compliant with the OpenAIRE guidelines).</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Backup, archive after the subscription ends</strong></p>
            </td>
            <td>
              <p>OpenAIRE will keep a backup of your gateway configuration after the end of your subscriptions.
                Just
                in
                case you change your mind and do not want to start from scratch.</p>
            </td>
          </tr>
          <tr class=" toggle" hidden>
            <td>
              <p class="uk-text-left"><strong>Trial Period</strong></p>
            </td>
            <td>
              <p>Try OpenAIRE CONNECT for free.</p>
            </td>
          </tr>
          </tbody>
        </table>

      </div>
    </div>

    <div class="uk-container uk-container-large uk-flex uk-flex-center">
      <div class="uk-section uk-text-center">
        <h4 class="uk-text-center uk-width-xlarge@m ">
          <div> Contact us to discover OpenAIRE CONNECT and the rest of OpenAIRE services<span
              class="uk-text-primary">.</span></div>

        </h4>
        <a class="uk-button uk-button-primary uk-margin-medium-top uk-margin-medium-bottom" routerLink="/contact-us">Contact
          us</a>
      </div>
    </div>

  `,
  styles: [`
      /*  #pricing-table td:nth-child(3) {
            !*border-right:  2px solid #211F7E;*!
            !*border-left:  2px solid #211F7E;*!
        background-image: linear-gradient(110deg,#feca1d 0,#fe9f1d 100%);
            color:black;
        }*/
      #pricing-table tr:last-child td:nth-child(3) {
          /*border-bottom:  2px solid #211F7E;*/
      }

      #pricing-table tr:first-child td:nth-child(3) {
          /*border-top:  2px solid #211F7E;*/
          /*background-color: #211F7E;*/
          /*color: white;*/
      }


  `]
})
export class PricingComponent implements OnInit, OnDestroy {

  public properties: EnvProperties = properties;

  private subscriptions: any[] = [];
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Subscriptions'}];

  constructor(private communityService: CommunityService,
              private seoService: SEOService,
              private _meta: Meta,
              private _router: Router,
              private _title: Title) {
  }

  ngOnInit() {
    /* Metadata */
    const url = properties.domain + properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, "property='og:url'");
    const description = "Subscriptions "
    const title = "Subscriptions "
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
    this._meta.updateTag({content: title}, "property='og:title'");
    this._title.setTitle(title);

  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
