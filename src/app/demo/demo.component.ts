import {Component, OnInit} from '@angular/core';
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {ActivatedRoute} from "@angular/router";
import {CustomizationOptions} from "../openaireLibrary/connect/community/CustomizationOptions";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {ResultPreview} from "../openaireLibrary/utils/result-preview/result-preview";
import {properties} from "../../environments/environment";
import {ResultLandingInfo} from "../openaireLibrary/utils/entities/resultLandingInfo";


@Component({
  selector: 'demo',
  templateUrl: 'demo.component.html',
})

export class DemoComponent implements OnInit{

  properties:EnvProperties;
  layout: CustomizationOptions = null;
  result: ResultPreview;
  resultLanding:ResultLandingInfo;
  constructor(private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.properties = properties;
    this.route.queryParams.subscribe(params => {
      if (params['layout']) {
        this.layout = JSON.parse(StringUtils.URIDecode(params['layout']));
      }
    });
    this.result =  new ResultPreview();
    this.resultLanding = new ResultLandingInfo();
    this.result.resultType = "Publication";
    this.resultLanding.types = this.result.types = ["Article"];
    this.resultLanding.accessMode = this.result.accessMode = "Open Access";
    this.result.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    this.resultLanding.description = this.result.description;
    this.resultLanding.authors = this.result.authors = [{fullName: "Lorem ipsum", orcid: "0000-1111-2222-3333", orcid_pending: null}, {fullName: "Lorem ipsum", orcid: null, orcid_pending: "0000-1111-2222-3333"},{fullName: "Lorem ipsum", orcid: null, orcid_pending: null}];
    this.resultLanding.title = this.result.title = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
    this.resultLanding.fundedByProjects = this.result.projects = [{id: "XYZ", acronym: "Project acronym", title: "Project name", funderShortname: "Funder acronym", funderName: "Funder name", code:" project code"}]
    this.resultLanding.countries  = this.result.countries = ["Greece"];
    this.resultLanding.languages = this.result.languages = ["GR"];
    this.resultLanding.identifiers = this.result.identifiers = new Map<string, string[]>();
    this.result.identifiers.set("doi",["DOI/1","DOI/2"]);
    this.resultLanding.date = this.result.year = "2020";
    this.resultLanding.publisher = "Publisher name";
    this.resultLanding.journal  = {journal: "Journal name", lissn: null};
    this.resultLanding.subjects = ["Subject 1", "Subject 2"];
    this.resultLanding.contexts = [{ "labelContext": "Research Community", "idContext": "",
      "labelCategory":"", "idCategory": "",
      "labelConcept": "", "idConcept": "" }];
    this.resultLanding.objIdentifier ="re3data_____::db814dc656a911b556dba42a331cebe9";


  }


}
