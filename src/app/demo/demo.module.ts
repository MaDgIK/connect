import {NgModule} from '@angular/core';
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {DemoComponent} from "./demo.component";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {TabsModule} from "../openaireLibrary/utils/tabs/tabs.module";
import {ResultPreviewModule} from "../openaireLibrary/utils/result-preview/result-preview.module";
import {ResultLandingModule} from "../openaireLibrary/landingPages/result/resultLanding.module";

@NgModule({
  imports: [
    HelperModule,
    CommonModule,
    RouterModule.forChild([
      {path: '', component: DemoComponent}
    ]),
    TabsModule,
    ResultPreviewModule,
    ResultLandingModule
  ],
  declarations: [
    DemoComponent
  ],
  exports: [
    DemoComponent
  ]
})


export class DemoModule{}
