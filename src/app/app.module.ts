import {APP_ID, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {OpenaireErrorPageComponent} from './error/errorPage.component';

import {AppRoutingModule} from './app-routing.module';
import {SharedModule} from './shared/shared.module';
import {CookieLawModule} from './openaireLibrary/sharedComponents/cookie-law/cookie-law.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {CommunitiesService} from './openaireLibrary/connect/communities/communities.service';
import {SubscribeModule} from './utils/subscribe/subscribe.module';
import {HttpInterceptorService} from "./openaireLibrary/http-interceptor.service";
import {InviteBasicModule} from "./utils/subscribe/invite/inviteBasic.module";
import {PageURLResolverModule} from "./openaireLibrary/utils/pageURLResolver.module";
import {Schema2jsonldModule} from "./openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {SubscribeService} from "./openaireLibrary/utils/subscribe/subscribe.service";
import {RoleVerificationModule} from "./openaireLibrary/role-verification/role-verification.module";
import {QuickContactModule} from "./openaireLibrary/sharedComponents/quick-contact/quick-contact.module";
import {AlertModalModule} from "./openaireLibrary/utils/modal/alertModal.module";
import {CustomizationService} from "./openaireLibrary/services/customization.service";
import {CommunityAccessGuard} from "./utils/communityAccess.guard";

@NgModule({
  
  imports: [
    SharedModule,
    CommonModule,
    HttpClientModule,
    ErrorModule,
    FormsModule,
    NavigationBarModule,
    BottomModule,
    CookieLawModule,
    SubscribeModule.forRoot(), InviteBasicModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PageURLResolverModule, Schema2jsonldModule, RoleVerificationModule, QuickContactModule, AlertModalModule
  ],
  declarations: [AppComponent, OpenaireErrorPageComponent],
  exports: [AppComponent],
  providers: [
    CommunitiesService, CustomizationService, CommunityAccessGuard, SubscribeService,
    {provide: APP_ID, useValue: 'serverApp'},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 10000}]
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
