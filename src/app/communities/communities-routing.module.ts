import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{CommunitiesComponent} from './communities.component';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: CommunitiesComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }
    ])
  ]
})
export class CommunitiesRoutingModule { }
