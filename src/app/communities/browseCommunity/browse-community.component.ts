import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {CommunityInfo} from '../../openaireLibrary/connect/community/communityInfo';
import {LocalStorageService} from "../../openaireLibrary/services/localStorage.service";
import {Subscriber} from "rxjs";
import {properties} from "../../../environments/environment";
import {StringUtils} from "../../openaireLibrary/utils/string-utils.class";
import {Visibility} from "../../openaireLibrary/monitor/entities/stakeholder";
import {AlertModal} from "../../openaireLibrary/utils/modal/alert";

@Component({
  selector: 'browse-community',
  templateUrl: 'browse-community.component.html'
})

export class BrowseCommunityComponent {
  @Input() public community: CommunityInfo = null;
  @Input() public showDescription: boolean = true;
  @Input() public smallTitle: boolean = false;
  @ViewChild('alertModal', { static: true }) modal: AlertModal;

  public hiddenMessage: string = "Community is hidden to registered users. It is visible only to users that have privileges to  manage community; delay: 100";
  // cut title too
  // check title length, if is manager, if is private and cut description accordingly
  public thresholdTitle: number = 45;
  public thresholdDescription: number = 120;
  properties: EnvProperties;
  public directLink: boolean = true;

  visibilityIcon: Map<Visibility, string> = new Map<Visibility, string> ([
    ["PRIVATE", 'incognito'],
    ["RESTRICTED", 'restricted']
  ]);

  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private localStorageService: LocalStorageService) {
  }


  public ngOnInit() {
    this.properties = properties;
    this.subscriptions.push(this.localStorageService.get().subscribe(value => {
      this.directLink = value;
    }));

  }

  subscriptions = [];
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }


  isProduction(): boolean {
    return this.properties.environment != "development";
  }

  getProductionPrefix(id:string): string {
    return (this.properties.environment == "production") ? "" : "beta.";
  }

  public confirmModalOpen() {
    this.modal.cancelButton = true;
    this.modal.okButton = true;
    this.modal.alertTitle = 'You are going to visit ' +
      ((this.community.title) ? this.community.title : this.community.shortTitle) + ' Gateway';
    this.modal.message = 'You will be navigated to a new tab. Are you sure that you want to proceed?'
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'Yes';
    this.modal.cancelButtonText = 'No';
    this.modal.choice = true;
    this.modal.open();
  }

  public getCommunityPageUrl(): string {
    let url = '';
    if (this.isProduction()) {
      url = 'https://' + this.getProductionPrefix(this.community.communityId) + this.community.communityId + '.openaire.eu';
    } else {
      url = this.router.createUrlTree(['/'], {
        queryParams: {'communityId': this.community.communityId}
      }).toString();
    }
    return url;
  }

  public goToCommunityPage(data: any) {
    if (data.value == true) {
      this.localStorageService.setCommunityDirectLink(data.choice);
      let url = '';
      if (this.isProduction()) {
        url = 'https://' + this.getProductionPrefix(this.community.communityId) + this.community.communityId + '.openaire.eu';
      } else {
        url = this.router.createUrlTree(['/'], {
          queryParams: {'communityId': this.community.communityId}
        }).toString();
      }
      window.open(url, '_blank');
    }
  }

  public _formatDescription(description) {
    let descr: string = StringUtils.HTMLToString(description);
    return (((descr).length > this.thresholdDescription) ? (descr.substring(0, (this.thresholdDescription - ('...').length)) + "...") : descr);
  }
}
