import {NgModule}                 from '@angular/core';
import {CommonModule}             from '@angular/common';
import {FormsModule}              from '@angular/forms';
import {RouterModule}             from '@angular/router';

import {BrowseCommunityComponent}     from './browse-community.component';

import {SearchFormModule}         from '../../openaireLibrary/searchPages/searchUtils/searchForm.module';
import {ManageModule}             from '../../openaireLibrary/utils/manage/manage.module';
import {AlertModalModule}         from "../../openaireLibrary/utils/modal/alertModal.module";
import {UrlPrefixModule} from "../../openaireLibrary/utils/pipes/url-prefix.module";
import {LogoUrlPipeModule} from "../../openaireLibrary/utils/pipes/logoUrlPipe.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../../openaireLibrary/utils/icons/icons.service";
import {incognito, restricted} from "../../openaireLibrary/utils/icons/icons";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SearchFormModule, ManageModule, AlertModalModule, UrlPrefixModule, LogoUrlPipeModule, IconsModule
  ],
  declarations: [
    BrowseCommunityComponent
  ],
  providers:[
  ],
  exports: [
    BrowseCommunityComponent
  ]
})
export class BrowseCommunityModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([incognito, restricted])
  }
}
