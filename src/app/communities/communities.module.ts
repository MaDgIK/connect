import {NgModule}                 from '@angular/core';
import {CommonModule}             from '@angular/common';
import {FormsModule}              from '@angular/forms';
import {RouterModule}             from '@angular/router';
import {ManageModule}             from '../openaireLibrary/utils/manage/manage.module';

import {CommunitiesComponent}     from './communities.component';

import {PreviousRouteRecorder}    from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {PiwikService}             from '../openaireLibrary/utils/piwik/piwik.service';
import {ErrorMessagesModule}      from '../openaireLibrary/utils/errorMessages.module';

import {SearchFormModule}         from '../openaireLibrary/searchPages/searchUtils/searchForm.module';
import {BrowseCommunityModule}    from './browseCommunity/browse-community.module';
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {OtherPortalsModule} from "../openaireLibrary/sharedComponents/other-portals/other-portals.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {SectionScrollModule} from "../openaireLibrary/utils/section-scroll/section-scroll.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {SliderUtilsModule} from "../openaireLibrary/sharedComponents/slider-utils/slider-utils.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    ManageModule, ErrorMessagesModule,
    SearchFormModule, BrowseCommunityModule, OtherPortalsModule,
    HelperModule, SEOServiceModule, SectionScrollModule, IconsModule, SliderUtilsModule
  ],
  declarations: [
    CommunitiesComponent
  ],
  providers:[
    PreviousRouteRecorder,
    PiwikService,
    IsRouteEnabled
  ],
  exports: [
    CommunitiesComponent
  ]
})
export class CommunitiesModule { }
