import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {GetStartedComponent} from './get-started.component';
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: GetStartedComponent, canActivate: [], canDeactivate: [PreviousRouteRecorder]}
        ])
    ]
})
export class GetStartedRoutingModule { }
