import {TestBed } from '@angular/core/testing';
import {properties} from "../environments/environment";

describe('Environment Configuration', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
    });
  });

  it('should have correct setting for piwik tracking based on environment', () => {
    if (properties.environment == "beta" || properties.environment == "production") {
      expect(properties.enablePiwikTrack).toBe(true,`Piwik tracking: Expected enablePiwikTrack to be  true but it is ${properties.enablePiwikTrack}`);
    } else {
      expect(properties.enablePiwikTrack).toBe(false, `Piwik tracking: Expected enablePiwikTrack to be false but it is ${properties.enablePiwikTrack}`);
    }
  });

  // Add more tests for other properties as needed
});
