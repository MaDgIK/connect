import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EmailService} from "../openaireLibrary/utils/email/email.service";
import {Email} from "../openaireLibrary/utils/email/email";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";
import {Composer} from "../openaireLibrary/utils/email/composer";
import {Meta, Title} from "@angular/platform-browser";
import {PiwikService} from "../openaireLibrary/utils/piwik/piwik.service";
import {HelperFunctions} from "../openaireLibrary/utils/HelperFunctions.class";
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {Subscriber} from "rxjs";
import {properties} from "../../environments/environment";
import {NotificationHandler} from "../openaireLibrary/utils/notification-handler";

@Component({
    selector: 'contact',
    templateUrl: './contact.component.html',
})

export class ContactComponent implements OnInit {
    public url: string = null;
    public pageTitle: string = "OpenAIRE - Connect | Contact Us";
    public description: string = "Contact us to learn more about OpenAIRE Connect Research Gateways";
    public sending = true;
    public email: Email;
    public properties: EnvProperties = properties;
    public pageContents = null;
    public divContents = null;
    public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'Contact us'}];
    public contactForm: UntypedFormGroup;
    @ViewChild('modal') modal;
    private subscriptions = [];
    
    ngOnDestroy() {
        this.subscriptions.forEach(subscription => {
            if (subscription instanceof Subscriber) {
                subscription.unsubscribe();
            }
        });
    }

    constructor(private route: ActivatedRoute,
                private _router: Router,
                private _emailService: EmailService,
                private _meta: Meta,
                private _title: Title,
                private seoService: SEOService,
                private _piwikService: PiwikService,
                private fb: UntypedFormBuilder,
                private helper: HelperService) {
    }

    ngOnInit() {
        this._title.setTitle('OpenAIRE-Connect | Contact Us');

        this.properties = properties;
        this.email = {body: '', subject: '', recipients: []};
        this.subscriptions.push( this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
        this.url = this.properties.domain + this._router.url;
        this.seoService.createLinkForCanonicalURL(this.url);
        this.updateUrl(this.url);
        this.updateTitle(this.pageTitle);
        this.updateDescription(this.description);
        this.reset();
        //this.getDivContents();
        this.getPageContents();
        this.sending = false;

    }

    private getPageContents() {
        this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
            this.pageContents = contents;
        }));
    }

    private getDivContents() {
        this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
            this.divContents = contents;
        }));
    }

    public send(event) {
        HelperFunctions.scroll();
        if(event.valid === true) {
            this.sendMail(this.properties.admins);
        }
    }

    public reset() {
        this.contactForm = this.fb.group( {
            name: this.fb.control('', Validators.required),
            surname: this.fb.control('', Validators.required),
            email: this.fb.control('', [Validators.required, Validators.email]),
            affiliation: this.fb.control('', Validators.required),
            community: this.fb.control('', Validators.required),
            message: this.fb.control('', Validators.required),
            recaptcha: this.fb.control('', Validators.required),
        })
    }

    private sendMail(admins: any) {
        this.sending = true;
        this.subscriptions.push(this._emailService.contact(this.properties,
            Composer.composeEmailForNewCommunity(this.contactForm.value, admins),
          this.contactForm.value.recaptcha).subscribe(
            res => {
                if (res) {
                    this.sending = false;
                    this.reset();
                    this.modalOpen();
                } else {
                    this.handleError('Email <b>sent failed!</b> Please try again.');
                }
            },
            error => {
                this.handleError('Email sent failed! Please try again.', error);
            }
        ));
    }

    public modalOpen() {
        this.modal.okButton = true;
        this.modal.alertTitle = 'Your request has been successfully submitted';
        this.modal.alertMessage = false;
        this.modal.cancelButton = false;
        this.modal.okButtonLeft = false;
        this.modal.okButtonText = 'OK';
        this.modal.open();
    }

    handleError(message: string, error = null) {
        if(error) {
            console.error(error);
        }
        NotificationHandler.rise(message, 'danger');
        this.sending = false;
        this.contactForm.get('recaptcha').setValue('');
    }

    public goToHome() {
        this._router.navigate(['/']);
    }

    private updateDescription(description: string) {
        this._meta.updateTag({content: description}, "name='description'");
        this._meta.updateTag({content: description}, "property='og:description'");
    }

    private updateTitle(title: string) {
        var _title = ((title.length > 50) ? title.substring(0, 50) : title);
        this._title.setTitle(_title);
        this._meta.updateTag({content: _title}, "property='og:title'");
    }

    private updateUrl(url: string) {
        this._meta.updateTag({content: url}, "property='og:url'");
    }
}
