import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {CommunitiesService} from '../openaireLibrary/connect/communities/communities.service';
import {CommunityInfo} from '../openaireLibrary/connect/community/communityInfo';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {Session, User} from '../openaireLibrary/login/utils/helper.class';
import {StringUtils} from '../openaireLibrary/utils/string-utils.class';

import {ErrorCodes} from '../openaireLibrary/utils/properties/errorCodes';
import {ErrorMessagesComponent} from '../openaireLibrary/utils/errorMessages.component';
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {properties} from "../../environments/environment";
import {Subscriber} from "rxjs";

@Component({
  selector: 'my-communities',
  templateUrl: 'my-communities.component.html',
})

export class MyCommunitiesComponent {
  public pageTitle = "OpenAIRE"
  public subscriberOfCommunities = [];
  public managerOfCommunities = [];
  public researchCommunities = [];
  public pageContents = null;
  public divContents = null;
  // Message variables
  public status: number;
  public loading: boolean = true;
  public subscriberErrorMessage: string = "";
  public errorCodes: ErrorCodes;
  private errorMessages: ErrorMessagesComponent;
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'My Communities'}];
  
  properties: EnvProperties;
  private user: User;
  subscriptions = [];
  
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _meta: Meta,
    private _title: Title,
    private _piwikService: PiwikService,
    private _communitiesService: CommunitiesService,
    private helper: HelperService,
    private seoService: SEOService,
    private userManagementService: UserManagementService) {
    
    var description = "OpenAIRE - Connect, Community Dashboard, research community " +
      "| My managing and subscribed to Communities";
    var title = "OpenAIRE - Connect | My Communities";
    
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
    this._meta.updateTag({content: title}, "property='og:title'");
    this._title.setTitle(title);
    
    this.errorCodes = new ErrorCodes();
    this.errorMessages = new ErrorMessagesComponent();
    this.status = this.errorCodes.LOADING;
  }
  
  public ngOnInit() {
    this.properties = properties;
    var url = this.properties.domain + this.properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, "property='og:url'");
    this.subscriptions.push(this._piwikService.trackView(this.properties, "OpenAIRE Connect").subscribe());
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      if (this.user) {
        this.getCommunities();
      }
      //this.getDivContents();
      //this.getPageContents();
    }));
    
  }
  
  private getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }
  
  hasPermission(communityInfo: CommunityInfo) {
    return communityInfo.isPublic() || (communityInfo.isRestricted() && communityInfo.isManager);
  }
  
  public getCommunities() {
    this.loading = true;
    this.status = this.errorCodes.LOADING;
    this.subscriberErrorMessage = "";
    
    this.subscriberOfCommunities = [];
    this.managerOfCommunities = [];
    this.researchCommunities = [];
    
    this.subscriptions.push(this._communitiesService.getCommunitiesState().subscribe(
      communitiesResults => {
        if (!communitiesResults) {
          return;
        }
        if (communitiesResults.length == 0) {
          this.status = this.errorCodes.DONE;
          return;
        }
        this.sort(communitiesResults);
        communitiesResults.forEach((community, index) => {
          let showCommunity: boolean = true;
          community.isManager = Session.isCommunityCurator(this.user) || Session.isPortalAdministrator(this.user) || Session.isManager('community', community.communityId, this.user);
          if (community.isPrivate()) {
            showCommunity = false;
          } else {
            if(!community.isManager && community.isRestricted()) {
              showCommunity = false;
            }
          }
          if (showCommunity) {
            this.researchCommunities.push(community);
            // if (community.isManager) {
            //   this.managerOfCommunities.push(community);
            // }
          }
          if (community.isManager) {
            this.managerOfCommunities.push(community);
          }
          this.status = this.errorCodes.DONE;
          if (this.user && showCommunity) {
            community.isSubscribed = Session.isSubscribedTo('community', community.communityId, this.user);
            if(community.isSubscribed) {
              if (community.isManager) {
                this.subscriberOfCommunities.push(community);
              } else {
                this.subscriberOfCommunities.unshift(community);
              }
            }
          }
          this.loading = false;
        });
      },
      error => {
        this.status = this.handleError("Error getting communities", error);
        this.loading = false;
      }
    ));
  }
  
  private sort(results: CommunityInfo[]) {
    results.sort((left, right): number => {
      if (!right.date || left.date > right.date) {
        return -1;
      } else if (!left.date || left.date < right.date) {
        return 1;
      } else {
        if (left.title > right.title) {
          return 1;
        } else if (left.title < right.title) {
          return -1;
        } else {
          return 0;
        }
      }
    })
  }
  
  public quote(param: string): string {
    return StringUtils.quote(param);
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  private handleError(message: string, error): number {
    var code = "";
    if (!error.status) {
      code = error.code;
    } else {
      code = error.status;
    }
    
    console.error("Communities (component): " + message, error);
    
    return this.errorMessages.getErrorCode(code);
  }
}
