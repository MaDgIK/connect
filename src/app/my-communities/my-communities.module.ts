import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ManageModule} from '../openaireLibrary/utils/manage/manage.module';

import {MyCommunitiesComponent} from './my-communities.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {ErrorMessagesModule} from '../openaireLibrary/utils/errorMessages.module';
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {BrowseCommunityModule} from "../communities/browseCommunity/browse-community.module";
import {MyCommunitiesRoutingModule} from "./my-communities-routing.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, HelperModule,
    ManageModule, ErrorMessagesModule, BrowseCommunityModule, Schema2jsonldModule, SEOServiceModule,
    MyCommunitiesRoutingModule, BreadcrumbsModule
  ],
  declarations: [
    MyCommunitiesComponent
  ],
  providers: [
    LoginGuard, PreviousRouteRecorder,
    PiwikService, IsRouteEnabled
  ],
  exports: [
    MyCommunitiesComponent
  ]
})
export class MyCommunitiesModule {
}
