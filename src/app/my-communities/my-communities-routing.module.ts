import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {MyCommunitiesComponent} from "./my-communities.component";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: MyCommunitiesComponent, canActivate: [LoginGuard, IsRouteEnabled], canDeactivate: [PreviousRouteRecorder]}

    ])
  ]
})
export class MyCommunitiesRoutingModule {
}
