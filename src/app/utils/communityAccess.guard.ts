import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {CommunityService} from "../openaireLibrary/connect/community/community.service";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {properties} from "../../environments/environment";
import {map, take} from "rxjs/operators";
import {LoginErrorCodes} from "../openaireLibrary/login/utils/guardHelper.class";
import {Session} from "../openaireLibrary/login/utils/helper.class";

@Injectable()
export class CommunityAccessGuard {

  constructor(private router: Router,
              private communityService: CommunityService, private userManagementService: UserManagementService) {
  }

  check(path: string): Observable<boolean> | boolean {
    let community = ConnectHelper.getCommunityFromDomain(properties.domain);
    if (!community) {
      this.router.navigate([properties.errorLink], {queryParams: {page: path}});
      return false;
    }
    return forkJoin([
      this.userManagementService.getUserInfo().pipe(take(1)),
      this.communityService.getCommunity(community).pipe(take(1))
    ]).pipe(
      map(([user, communityInfo]) => {
        if (communityInfo) {
          if (communityInfo.isPublic()) {
            return true;
          } else if (communityInfo.isPrivate()) {
            this.router.navigate([properties.errorLink], {queryParams: {page: path}});
            return false;
          } else {
            if (!user) {
              this.router.navigate(['/user-info'], {
                queryParams: {
                  'errorCode': LoginErrorCodes.NOT_LOGIN,
                  'redirectUrl': path
                }
              })
              return false;
            }
            if (Session.isPortalAdministrator(user) || Session.isCommunityCurator(user) || Session.isManager('community', community, user) || Session.isSubscribedTo('community', community, user)) {
              return true;
            } else {
              this.router.navigate(['/user-info'], {
                queryParams: {
                  'errorCode': LoginErrorCodes.NOT_AUTHORIZED,
                  'redirectUrl': path
                }
              })
              return false;
            }

          }
        }
        this.router.navigate([properties.errorLink], {queryParams: {page: path}});
        return false;

      })
    );
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.check(state.url);
  }
}
