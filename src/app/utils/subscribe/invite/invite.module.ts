import {NgModule} from '@angular/core';

import {InviteRoutingModule} from './invite-routing.module';
import {InviteBasicModule} from "./inviteBasic.module";
import {ConnectAdminLoginGuard} from "../../../openaireLibrary/connect/communityGuard/connectAdminLoginGuard.guard";

@NgModule({
  imports: [InviteBasicModule, InviteRoutingModule],
  providers: [ConnectAdminLoginGuard]
})

export class InviteModule { }
