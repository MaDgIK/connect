import {NgModule}                   from '@angular/core';
import {CommonModule}               from '@angular/common';
import {FormsModule}                from '@angular/forms';
import {RouterModule}               from '@angular/router';
import {CKEditorModule}             from 'ng2-ckeditor';

import {InviteComponent}            from './invite.component';

import {PreviousRouteRecorder}      from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {EmailService}               from '../../../openaireLibrary/utils/email/email.service';
import {ErrorMessagesModule}        from '../../../openaireLibrary/utils/errorMessages.module';
import {IsRouteEnabled} from "../../../openaireLibrary/error/isRouteEnabled.guard";
import {HelperModule} from "../../../openaireLibrary/utils/helper/helper.module";
import {Schema2jsonldModule} from "../../../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../../../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {PiwikService} from "../../../openaireLibrary/utils/piwik/piwik.service";
import {BreadcrumbsModule} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {SubscriberInviteModule} from "../../../openaireLibrary/sharedComponents/subscriber-invite/subscriber-invite.module";
import {LoadingModule} from "../../../openaireLibrary/utils/loading/loading.module";
import {IconsService} from "../../../openaireLibrary/utils/icons/icons.service";
import {cog} from "../../../openaireLibrary/utils/icons/icons";
import {IconsModule} from "../../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [
    RouterModule, CommonModule, FormsModule, CKEditorModule, ErrorMessagesModule,
    HelperModule, Schema2jsonldModule, SEOServiceModule, BreadcrumbsModule, SubscriberInviteModule, LoadingModule, IconsModule
  ],
  declarations: [
    InviteComponent
  ],
  providers: [
    PreviousRouteRecorder,
    EmailService, IsRouteEnabled,
    PiwikService
  ],
  exports: [
    InviteComponent
  ]
})

export class InviteBasicModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([cog])
  }
}
