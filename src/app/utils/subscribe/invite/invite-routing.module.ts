import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {InviteComponent} from './invite.component';
import {PreviousRouteRecorder} from '../../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {ConnectAdminLoginGuard} from "../../../openaireLibrary/connect/communityGuard/connectAdminLoginGuard.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: InviteComponent, canActivate: [ConnectAdminLoginGuard], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class InviteRoutingModule { }
