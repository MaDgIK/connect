import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {EnvProperties} from '../../../openaireLibrary/utils/properties/env-properties';
import {User} from '../../../openaireLibrary/login/utils/helper.class';
import {HelperService} from "../../../openaireLibrary/utils/helper/helper.service";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {PiwikService} from "../../../openaireLibrary/utils/piwik/piwik.service";
import {UserManagementService} from "../../../openaireLibrary/services/user-management.service";
import {Breadcrumb} from "../../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {Subscriber, Subscription} from "rxjs";
import {properties} from "../../../../environments/environment";

declare var UIkit;

@Component({
  selector: 'invite',
  templateUrl: './invite.component.html',
})
export class InviteComponent implements OnInit {
  @Input() longView: boolean = true;
  @Input() communityId = null;
  @Input() isOpen: boolean = true;
  public properties: EnvProperties = properties;
  public pageTitle: string = "Invite";
  public user: User;
  public url: string;
  public pageContents;
  public divContents
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'invite'}];
  private subs: Subscription[] = [];
  
  constructor(
    private _router: Router,
    private _meta: Meta,
    private helper: HelperService,
    private _title: Title,
    private seoService: SEOService,
    private userManageService: UserManagementService) {
  }
  
  public ngOnInit() {
    this.subs.push(this.userManageService.getUserInfo().subscribe(
      user => {
        this.user = user;
    }));
    if(this.longView) {
      this.url = this.properties.domain + this._router.url;
      this.getPageContents();
      this.seoService.createLinkForCanonicalURL(this.url);
      this.updateUrl(this.url);
      this.updateTitle(this.pageTitle);
      this.updateDescription("OpenAIRE - Connect, Community Gateway, research community, invite");
    }
  }
  
  ngOnDestroy() {
    for (let sub of this.subs) {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    }
  }
  
  private getPageContents() {
    this.subs.push(this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  private getDivContents() {
    this.subs.push(this.helper.getDivHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }
  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }

  get adminInviteURL() {
    return this.properties.adminPortalURL + '/' + this.communityId + '/users/member';
  }
  
  close(element) {
    UIkit.dropdown(element).hide();
  }
}
