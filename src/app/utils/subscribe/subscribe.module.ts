import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {EmailService} from "../../openaireLibrary/utils/email/email.service";
import {SubscribeComponent} from './subscribe.component';
import {AlertModalModule} from '../../openaireLibrary/utils/modal/alertModal.module';
import {LoadingModule} from "../../openaireLibrary/utils/loading/loading.module";
import {IconsModule} from "../../openaireLibrary/utils/icons/icons.module";

@NgModule({
  imports: [
    CommonModule, RouterModule, AlertModalModule, LoadingModule, IconsModule
  ],
  declarations: [
    SubscribeComponent
  ],
  exports: [
    SubscribeComponent
  ]
})
export class SubscribeModule {
  static forRoot(): ModuleWithProviders<SubscribeModule> {
    return {
      ngModule: SubscribeModule,
      providers: [
        EmailService
      ]
    }
  }
}
