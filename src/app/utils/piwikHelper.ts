/**
 * @deprecated
 * */
export class PiwikHelper{
  public static siteIDs = {
    "dh-ch":81,
    "sdsn-gr":82,
    "egi":83,
    "elixir-gr":84,
    "fam":85,
    "instruct":86,
    "mes":87,
    "ni":88,
    "oa-pg":89,
    "rda":90,
    "aginfra":93,
    "clarin":100,
    "dariah":103,
    "epos": 217,
    "beopen": 218,
    "risis":219,
    "science-innovation-policy":253,
    "covid-19":267,
    "rural-digital-europe":319,
    "enermaps":318,
    "galaxy":453,
    "gotriple":490,
    "neanias-underwater":474,
    "embrc": 505,
    "inspired-ris":530,
    "citizen-science":538,
    "eut":558,
    "aurora": 560,
    "ebrains": 592,
    "neanias-space": 604,
    "heritage-science": 607,
    "eutopia": 608,
    "north-america-studies": 609,
    "iperionhs": 610,
    "neanias-atmospheric": 613,
    "forthem": 625,
    "argo-france": 634,
    "knowmad": 640,
    "egrise": 710,
    "euconexus": 707,
    "dth": 719,
    "lifewatch-eric": 744
  };
  
  public static getSiteId(communityId:string){
    return this.siteIDs[communityId];
  }
}
