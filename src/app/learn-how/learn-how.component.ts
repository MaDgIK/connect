import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {Subscriber} from "rxjs";
import {properties} from "../../environments/environment";
import {QuickContactService} from '../openaireLibrary/sharedComponents/quick-contact/quick-contact.service';
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";

@Component({
  selector: 'learn-how',
  template: `
    <schema2jsonld *ngIf="url" [URL]="url" [name]="pageTitle" type="other"></schema2jsonld>
		<div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
			<div class="uk-padding-small uk-padding-remove-horizontal">
				<breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
			</div>
		</div>
    <div class="uk-section uk-padding-remove-top">
      <div class="uk-container uk-container-large uk-margin-top uk-margin-large-bottom">
        <p>
        OpenAIRE CONNECT is a platform-as-a-service that enables research communities, research performing organizations
        and research infrastructures to create customized web portals for discovering and sharing their research
        outcomes and Open Science practices. It supports both public and private settings, integrates Open Science for
        enhanced collaboration, and offers rapid setup and branding customization. In short, a virtual gateway for your
        community, however you define it.
        </p>
        <p>
        OpenAIRE CONNECT significantly improves the visibility and dissemination of research within and beyond the
        community, connecting it to the wider scientific landscape.
        </p>
        <p>
        This service was created for use by research communities that want to gather their research outcomes, currently
        scattered across multiple repositories and archives, into a single entry-point, such as:
        </p>
        <ul>


          <li>Research projects, that want research outcomes related to a specific research topic to be collected at a
            single entry-point
          </li>

          <li>Research infrastructures, that want to track the research outcomes produced thanks to the resources they
            offer
          </li>
          <li>
            University alliances, that require a single entry-point to all research outcomes of their alliance members
          </li>
        </ul>
      </div>
    </div>

    <div class="uk-section uk-padding-remove-top pattern-background">
			<div class="uk-container uk-container-large uk-margin-top uk-margin-large-bottom">
				<div class="uk-grid uk-grid-large" uk-grid>
					<div class="uk-width-1-3@m uk-width-1-1">
						<h1>
							Learn the process<span class="uk-text-primary">.</span>
						</h1>
						<div class="uk-width-3-4 uk-text-large">
							Build a <span class="uk-text-secondary uk-text-bold">Gateway to your community's</span> open and linked research outputs. Customized to your needs.
						</div>
					</div>
					<div class="uk-width-expand">
            <ul class="uk-tab" uk-tab>
              <li *ngFor="let step of steps; let i=index" [class.uk-active]="i === activeStep">
                <a class="uk-flex uk-flex-column uk-flex-middle" (click)="activeStep = i">
                  <icon [svg]="step.icon" ratio="1.5" [customClass]="i === activeStep?'uk-text-primary':null" class="uk-margin-bottom"></icon>
                  <span>{{step.name}}</span>
                </a>
              </li>
            </ul>
            <div *ngIf="activeStep == 0">
                <div class="uk-margin-medium-bottom uk-margin-large-top">
                  You don’t have to go alone.
                </div>
                <div class="uk-margin-medium-bottom">
                  We work with you in <span class="uk-text-secondary uk-text-bold">4 collaborative steps</span> to identify your needs, putting in practice our expertise on open science so you get the most out of OpenAIRE’s operational services.
                </div>
                <div class="uk-grid uk-child-width-1-2 uk-grid-large" uk-grid>
                  <div>
                    <div class="uk-margin-small-bottom uk-text-large uk-text-bold">
                      1. Understanding your needs
                    </div>
                    <div>
                      First, we learn about your requirements and challenges. We help you understand Open Science practices within EOSC and together we’ll talk about how OpenAIRE RCD fits as a solution.
                    </div>
                  </div>
                  <div>
                    <div class="uk-margin-small-bottom uk-text-large uk-text-bold">
                      2. Develop a pilot
                    </div>
                    <div>
                      How do you work today, and how would you like to work tomorrow? We translate your needs into rules and processes and we configure operational OpenAIRE services. By the end of this phase, we’ll have defined the basic configuration of your Community Gateway.
                    </div>
                  </div>
                  <div>
                    <div class="uk-margin-small-bottom uk-text-large uk-text-bold">
                      3. Test and Validate
                    </div>
                    <div>
                      You validate and test your new Community Gateway (portal) with your experts and community to ensure all workflows are in place and quality of data meets your standards. If needed, we work together in another iteration to further refine and adapt to your needs.
                    </div>
                  </div>
                  <div>
                    <div class="uk-margin-small-bottom uk-text-large uk-text-bold">
                      4. Roll out the service
                    </div>
                    <div>
                      We jointly roll out your new Community Gateway. You take over the business operations and start engaging your researchers, we take care of the smooth operation of the e-service.
                    </div>
                  </div>
                </div>
              </div>
            <div *ngIf="activeStep == 1">
              <div class="uk-margin-large-top uk-margin-small-bottom uk-text-large uk-text-bold">
                1. Understanding your needs
              </div>
              <div class="uk-margin-medium-bottom">
                <i>
                  "Identify the scope and goals. Understand open science practices within EOSC and the OpenAIRE services."
                </i>
              </div>
              <div class="uk-margin-medium-bottom">
                In this stage, you get to talk to the OpenAIRE team. Share your expectations with us and let us give you all the details about the operational OpenAIRE services, which will be integrated into the Gateway for your community.
              </div>
              <div class="uk-margin-medium-bottom">
                Here are the most important questions that the OpenAIRE team will ask you, in order to understand your scope and goals:
              </div>
              <ul class="uk-list uk-list-bullet uk-list-primary">
                <li class="uk-margin-small-bottom">
                  Do you want a gateway, where researchers can have access to all research products of a discipline? Do you want a gateway that gathers any research outcome, produced thanks to the funding and services of a given research infrastructure?
                </li>
                <li class="uk-margin-small-bottom">
                  Is your community (in)formally organized in sub-communities? Would you like to browse research products and get statistics also for these sub-communities? For example, the European Grid Infrastructure (EGI) features “virtual organizations” that represent discipline-specific communities and/or specific research projects. The research infrastructure DARIAH, on the other hand, is organised in national nodes (e.g. DARIAH-IT, DARIAH-DE).
                </li>
                <li class="uk-margin-small-bottom">
                  How can the OpenAIRE team identify the research products of your community, among all those available in the OpenAIRE Graph? Through a series of steps: set of keywords, acknowledgment statements, set of projects, set of repositories, etc. This can be partial and provisional information that will serve as a starting point to the OpenAIRE team. You will be able to refine and update this information, in the second phase “Develop a pilot”.
                </li>
              </ul>
            </div>
            <div *ngIf="activeStep == 2">
              <div class="uk-margin-large-top uk-margin-small-bottom uk-text-large uk-text-bold">
                2. Develop a pilot
              </div>
              <div class="uk-margin-medium-bottom">
                <i>
                  "We translate your needs into rules and processes and we configure operational OpenAIRE services."
                </i>
              </div>
              <div class="uk-margin-medium-bottom">
                Based on the information gathered in phase 1 “Analyse your needs”, the OpenAIRE team will set up a pilot Gateway. We will configure the OpenAIRE mining algorithms to identify research products of the OpenAIRE Graph that are relevant to your community. Those, together with some basic statistics, will be available in the pilot version of the Community Gateway that will be deployed on the OpenAIRE BETA infrastructure.
              </div>
              <div>
                The OpenAIRE team will give you a demo of the Community Gateway, with details on how to refine and update the configuration of the Community Gateway, both in terms of criteria for including research products and in terms of logo and visible portal pages.
              </div>
            </div>
            <div *ngIf="activeStep == 3">
              <div class="uk-margin-large-top uk-margin-small-bottom uk-text-large uk-text-bold">
                3. Test and Validate
              </div>
              <div class="uk-margin-medium-bottom">
                <i>
                  "You validate and test your new Community Gateway (portal). If needed, we further refine and adapt to your needs."
                </i>
              </div>
              <div class="uk-margin-medium-bottom">
                Upon the completion of phase 2, take the time you need to test all its features, from search and browse for research products, to addition/removal of statistics from the portal. You can report any issue you might find and ask questions directly to the dedicated OpenAIRE team, via a specially designed collaboration tool.
              </div>
              <div class="uk-margin-medium-bottom">
                Typically, this phase takes some months, as you will have to go through certain procedures. Change the configuration of the criteria to include research products, wait for the new configuration to be applied on the OpenAIRE graph and validate the results, before you actually decide that the coverage of research products for your community is adequate.
              </div>
              <div>
                For some communities, the OpenAIRE team may also be able to implement dedicated mining algorithms (e.g. to find acknowledgement statements to your community/infrastructure in the full-texts of research articles) that may require several rounds of application, validation, and fine-tuning, before it reaches a high precision and recall. Your feedback is very important to minimize the effort and time needed for this process to complete.
              </div>
            </div>
            <div *ngIf="activeStep == 4">
              <div class="uk-margin-large-top uk-margin-small-bottom uk-text-large uk-text-bold">
                4. Roll out the service
              </div>
              <div class="uk-margin-medium-bottom">
                <i>
                  "We jointly roll out your new portal. You take over the business operations and start engaging your researchers."
                </i>
              </div>
              <div class="uk-margin-medium-bottom ">
                Here we are: the coverage of research products is good, interesting statistics and charts have been selected, and the portal pages available for end-users are ready. We can roll out the Community Gateway and make it available to all the researchers of the community!
              </div>
              <div class="uk-margin-medium-bottom ">
                You, as a Community manager, become the main “promoter” of the Community Gateway. Engage the researchers of your community and, when applicable, inform the managers of the research infrastructure about the availability of tools for impact monitoring.
              </div>
              <div class="uk-margin-medium-bottom ">
                Remember that you will still be able to change the configuration of the Community Gateway in order to address any issue that may arise and to follow the evolution of the community (e.g. a new project or a new content provider that was not previously available in OpenAIRE).
              </div>
              <div>
                Remember that you don’t have to go alone: the dedicated issue tracker you used in the “Test and Validate” phase is always available for you to contact the OpenAIRE team and ask for support.
              </div>
            </div>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-section">
			<div class="uk-container uk-container-large uk-margin-top">
				<div class="uk-margin-large-bottom uk-width-2-5@m">
					<h2 class="uk-h1">Find the best for your community<span class="uk-text-primary">.</span></h2>
				</div>
				<div class="uk-grid uk-grid-large uk-child-width-1-3" uk-grid>
					<div class="left">
						<div class="left-text uk-margin-medium-bottom uk-padding uk-margin-medium-left uk-padding-remove-right">
							<h6 class="uk-margin-small-bottom">Profile</h6>
							<div>Edit community information, change logo url, add community managers or organizations related to community.</div>
						</div>
						<div class="left-text uk-margin-medium-bottom uk-padding" >
							<h6 class="uk-margin-small-bottom">Content</h6>
							<div>Manage projects, content providers, subjects and zenodo communities that are related to the research community.</div>
						</div>
						<!--<div class="left-text uk-margin-medium-bottom uk-padding uk-margin-medium-left uk-padding-remove-right">
							<h6 class="uk-margin-small-bottom">Statistics & Charts</h6>
							<div>Manage statistical numbers & charts that will be displayed in the community overview and graph analysis views.</div>
						</div>-->
					</div>
					<div class="mid uk-flex uk-flex-middle">
            <div class="uk-position-relative">
            <img class="uk-position-center uk-position-z-index uk-position-absolute" src="assets/connect-assets/home/tablet.png" alt="ipad"
                 loading="lazy"
                 style="width: 100%;">
						<img src="assets/connect-assets/home/3.png" alt="">
            </div>
					</div>
					<div class="right">
						<div class="right-text uk-margin-medium-bottom uk-padding uk-margin-right-left uk-padding-remove-left">
							<h6 class="uk-margin-small-bottom">Links</h6>
							<div>Manage user claims related to the research community.</div>
						</div>
						<div class="right-text uk-margin-medium-bottom uk-padding">
							<h6 class="uk-margin-small-bottom">Help texts</h6>
							<div>Add or edit help text in research community pages.</div>
						</div>
						<div class="right-text uk-margin-medium-bottom uk-padding uk-margin-right-left uk-padding-remove-left">
							<h6 class="uk-margin-small-bottom">Users</h6>
							<div>Invite more users to subscribe, manage community subscribers, your personal info and notification settings.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <div class="uk-section uk-section-large uk-container uk-container-large">
      <div class="uk-margin-medium-top">
        <h2 class="uk-h1">
          Our mission for an Open and FAIR science<span class="uk-text-primary">.</span>
        </h2>
        <div class="uk-margin-large-top">
          <div class="uk-child-width-1-3@m uk-child-width-1-1@s uk-grid-large uk-grid-match uk-grid" uk-grid=""
               uk-height-match=".target">
            <div class="uk-first-column">
              <div class="uk-card uk-card-default uk-padding">
                <div class="uk-card-media-top uk-flex uk-flex-middle uk-flex-center
\t\t\t\t\t\t\t\tuk-padding-small uk-padding-remove-vertical uk-margin-top uk-margin-bottom">
                  <div>
                    <img src="assets/connect-assets/home/virtual.svg"
                         alt="A Virtual Research Environment" style="height:60px; width:67px"/>
                  </div>
                </div>
                <div class="uk-card-body uk-padding-remove uk-margin-small-bottom">
                  <div class="target uk-text-center">
                    <h3 class="uk-6 uk-card-title uk-margin-small-bottom">A Virtual Research
                      Environment</h3>
                    <div>An overlay platform making it easy to share, link, disseminate and monitor all
                      your {{entities.PUBLICATIONS | lowercase}},
                      {{entities.DATASETS | lowercase}}, {{entities.SOFTWARE | lowercase}}, methods.
                      In one place.
                    </div>
                  </div>
                  <hr/>
                  <div>
                    <ul class="uk-list uk-text-small">
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Access to OpenAIRE research graph
                      </li>
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Moderated, front-end linking
                      </li>
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Cross-platform search
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div class="uk-card uk-card-default uk-padding">
                <div class="uk-card-media-top uk-flex uk-flex-middle uk-flex-center
\t\t\t\t\t\t\t\t\t\t\t\t\tuk-padding-small uk-padding-remove-vertical uk-margin-top uk-margin-bottom">
                  <div>
                    <img src="assets/connect-assets/home/open_science.svg" alt="Open science in action"
                         style="height:60px; width:67px"/>
                  </div>
                </div>
                <div class="uk-card-body uk-padding-remove uk-margin-small-bottom">
                  <div class="target uk-text-center" style="">
                    <h3 class="uk-h6 uk-card-title uk-margin-small-bottom">Open Science in action</h3>
                    <div>A time-saving bundle of services for researchers to effortlessly practice open
                      science. An integral
                      part of the&nbsp;European Open Science Cloud.
                    </div>
                  </div>
                  <hr>
                  <div>
                    <ul class="uk-list uk-text-small">
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Use of OpenAIRE Guidelines
                      </li>
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Publish and get DOIs with Zenodo
                      </li>
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        EOSC Single Sign-On
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div class="uk-card uk-card-default uk-padding">
                <div class="uk-card-media-top uk-flex uk-flex-middle uk-flex-center
\t\t\t\t\t\t\t\t\t\t\t\t\tuk-padding-small uk-padding-remove-vertical uk-margin-top uk-margin-bottom">
                  <div>
                    <img src="assets/connect-assets/home/customize.svg" alt="Customized to your needs"
                         style="height:60px; width:67px"/>
                  </div>
                </div>
                <div class="uk-card-body uk-padding-remove uk-margin-small-bottom">
                  <div class="target uk-text-center" style="">
                    <h3 class="uk-h6 uk-card-title uk-margin-small-bottom">Customized to your needs</h3>
                    <div>A Gateway with your own brand, rules for aggregation, text &amp; data mining,
                      and presentation. Run
                      by you via a simple, yet powerful backend administration tool.
                    </div>
                  </div>
                  <hr>
                  <div>
                    <ul class="uk-list uk-text-small">
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Access control
                      </li>
                      <!--<li class="uk-flex uk-flex-middle">
                          <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85" flex="true"></icon>
                          Analytics: rich set of indicators
                      </li>-->
                      <li class="uk-flex uk-flex-middle">
                        <icon class="uk-margin-small-right uk-text-primary" name="done" ratio="0.85"
                              flex="true"></icon>
                        Look &amp; feel to match your brand
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
		<div #contact class="uk-section uk-container uk-container-large uk-text-center uk-flex uk-flex-column uk-flex-middle">
			<h2 class="uk-h2 uk-margin-medium-top uk-width-3-5@m">We look forward to working together and helping you unlock the full potential of your research community through open science<span class="uk-text-primary">.</span></h2>
			<a class="uk-button uk-button-primary uk-text-uppercase uk-margin-medium-top uk-margin-medium-bottom" routerLink="/contact-us">Contact us</a>
		</div>
  `,
  styleUrls: ['learn-how.component.less']
})
export class LearnHowComponent implements OnInit {
  public pageContents = null;
  public divContents = null;
  public activeStep = 0;
  public entities = OpenaireEntities;

  steps: any[] = [
    {name: 'All 4 Steps', icon: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 234.82 236.48"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><circle class="cls-1" cx="48.45" cy="48.45" r="48.45"/><circle class="cls-1" cx="186.37" cy="48.45" r="48.45"/><circle class="cls-1" cx="48.45" cy="188.03" r="48.45"/><circle class="cls-1" cx="186.37" cy="188.03" r="48.45"/></g></g></svg>'},
    {name: '1st Step', icon: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 227.96 265.77"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M195.82,3.62h-29V27h29a7.74,7.74,0,0,1,8,7.48V234.88a7.73,7.73,0,0,1-8,7.47H32.14a7.73,7.73,0,0,1-8-7.47V34.52a7.74,7.74,0,0,1,8-7.48h29V3.62h-29C14.42,3.62,0,17.48,0,34.52V234.88c0,17,14.42,30.89,32.14,30.89H195.82c17.72,0,32.14-13.86,32.14-30.89V34.52C228,17.48,213.54,3.62,195.82,3.62Z"/><path class="cls-1" d="M75.47,30.67h77a4.7,4.7,0,0,0,4.6-4.79V4.78A4.7,4.7,0,0,0,152.48,0h-77a4.7,4.7,0,0,0-4.6,4.78v21.1A4.7,4.7,0,0,0,75.47,30.67Z"/><rect class="cls-1" x="49.23" y="122.53" width="42.79" height="30.11" rx="5.57"/><polygon class="cls-1" points="173.33 99.65 151.52 129.09 147.3 124.17 136.14 133.74 152.37 152.65 185.14 108.39 173.33 99.65"/></g></g></svg>'},
    {name: '2nd Step', icon: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 227.96 265.77"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M162.75,210.23V176.77L137.8,150S142.05,85.75,114,65.62C85.91,85.75,90.16,150,90.16,150l-25,26.8v33.46s39.13-33.17,32.32-23.82.57,16.45.57,16.45l-8.51,9.64h48.77l-8.5-9.64s7.37-7.09.56-16.45S162.75,210.23,162.75,210.23Z"/><path class="cls-1" d="M195.82,3.62h-29V27h29a7.74,7.74,0,0,1,8,7.48V234.88a7.73,7.73,0,0,1-8,7.47H32.14a7.73,7.73,0,0,1-8-7.47V34.52a7.74,7.74,0,0,1,8-7.48h29V3.62h-29C14.42,3.62,0,17.48,0,34.52V234.88c0,17,14.42,30.89,32.14,30.89H195.82c17.72,0,32.14-13.86,32.14-30.89V34.52C228,17.48,213.54,3.62,195.82,3.62Z"/><path class="cls-1" d="M76.4,30.67h75.15A4.65,4.65,0,0,0,156,25.88V4.78A4.65,4.65,0,0,0,151.55,0H76.4a4.65,4.65,0,0,0-4.49,4.78v21.1A4.66,4.66,0,0,0,76.4,30.67Z"/></g></g></svg>'},
    {name: '3rd Step', icon: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 293.74 263.39"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M260.75,0H33A33,33,0,0,0,0,33V176.61a33,33,0,0,0,33,33h74.4v23h-19v30.82H203.66V232.57H184.44v-23h76.31a33,33,0,0,0,33-33V33A33,33,0,0,0,260.75,0Zm3.75,174.83a7,7,0,0,1-6.86,7.1H36.1a7,7,0,0,1-6.86-7.1V34.76a7,7,0,0,1,6.86-7.09H257.64a7,7,0,0,1,6.86,7.09Z"/><path class="cls-1" d="M260.75,0H33A33,33,0,0,0,0,33V176.61a33,33,0,0,0,33,33h74.4v23h-19v30.82H203.66V232.57H184.44v-23h76.31a33,33,0,0,0,33-33V33A33,33,0,0,0,260.75,0Zm3.75,174.83a7,7,0,0,1-6.86,7.1H36.1a7,7,0,0,1-6.86-7.1V34.76a7,7,0,0,1,6.86-7.09H257.64a7,7,0,0,1,6.86,7.09Z"/><path class="cls-1" d="M260.75,0H33A33,33,0,0,0,0,33V176.61a33,33,0,0,0,33,33h74.4v23h-19v30.82H203.66V232.57H184.44v-23h76.31a33,33,0,0,0,33-33V33A33,33,0,0,0,260.75,0Zm3.75,174.83a7,7,0,0,1-6.86,7.1H36.1a7,7,0,0,1-6.86-7.1V34.76a7,7,0,0,1,6.86-7.09H257.64a7,7,0,0,1,6.86,7.09Z"/><path class="cls-1" d="M123,155.32V132.26l-17.19-18.47s2.93-44.26-16.42-58.13C70.05,69.53,73,113.79,73,113.79l-17.2,18.47v23.06s27-22.86,22.28-16.41.39,11.33.39,11.33l-5.86,6.65h33.61l-5.87-6.65s5.09-4.88.4-11.33S123,155.32,123,155.32Z"/><rect class="cls-1" x="164.94" y="117.66" width="15.88" height="39.22"/><rect class="cls-1" x="215.69" y="99.44" width="15.88" height="57.44"/><rect class="cls-1" x="190.27" y="75.62" width="15.88" height="81.26"/></g></g></svg>'},
    {name: '4th Step', icon: '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 177.86 248.05"><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M177.86,236.27V197.84A25.23,25.23,0,0,0,170.48,180l-38.12-38.11s7.4-104-40-140.7a5.62,5.62,0,0,0-6.92,0c-47.37,36.67-40,140.7-40,140.7L7.39,180A25.24,25.24,0,0,0,0,197.84v38.43a3.86,3.86,0,0,0,6.23,3c17.1-13.33,62.85-48.53,52.71-35.55-12.41,15.88,1,27.92,1,27.92L47.13,245.23a1.67,1.67,0,0,0,1.21,2.82h81.18a1.67,1.67,0,0,0,1.21-2.82l-12.84-13.55s13.44-12,1-27.92c-10.13-13,35.61,22.22,52.71,35.55A3.86,3.86,0,0,0,177.86,236.27Z"/></g></g></svg>'},
  ];

  public url: string = null;
  public pageTitle: string = "OpenAIRE - Connect | Learn How";
  public pageDescription: string = "Learn the process: Build a Gateway to your community's open and linked research outcomes. Customized to your needs.";
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'About'}];
  properties: EnvProperties = properties;
	public showQuickContact: boolean;
	@ViewChild('contact') contact: ElementRef;
  subscriptions = [];
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _meta: Meta,
    private _title: Title,
    private seoService: SEOService,
    private _piwikService: PiwikService,
    private helper: HelperService,
		private quickContactService: QuickContactService) {
  }
  
  public ngOnInit() {
        this.subscriptions.push(this._piwikService.trackView(this.properties, this.pageTitle).subscribe());
        this.url = this.properties.baseLink + this._router.url;
        this.seoService.createLinkForCanonicalURL(this.url);
        this.updateUrl(this.url);
        this.updateTitle(this.pageTitle);
        this.updateDescription(this.pageDescription);
        //this.getDivContents();
        this.getPageContents();
  }
  
  private getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  private getDivContents() {
    this.subscriptions.push(this.helper.getDivHelpContents(this.properties, 'connect', this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      } else if(typeof IntersectionObserver !== 'undefined' && subscription instanceof IntersectionObserver) {
        subscription.disconnect();
      }
    });
  }

	ngAfterViewInit() {
    if (typeof IntersectionObserver !== "undefined") {
      this.createObservers();
    }
  }

	createObservers() {
		let options = {
      root: null,
      rootMargin: '200px',
      threshold: 1.0
    };
    let intersectionObserver = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting && this.showQuickContact) {
          this.showQuickContact = false;
          this.quickContactService.setDisplay(this.showQuickContact);
        } else if (!entry.isIntersecting && !this.showQuickContact) {
          this.showQuickContact = true;
          this.quickContactService.setDisplay(this.showQuickContact);
        }
      });
    }, options);
		intersectionObserver.observe(this.contact.nativeElement);
		this.subscriptions.push(intersectionObserver);
  }

  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
}
