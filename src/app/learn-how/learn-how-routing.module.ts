import {NgModule}     from '@angular/core';
import {RouterModule} from '@angular/router';
import {PreviousRouteRecorder}  from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {LearnHowComponent} from "./learn-how.component";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: LearnHowComponent, canActivate: [ IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class LearnHowRoutingModule { }
