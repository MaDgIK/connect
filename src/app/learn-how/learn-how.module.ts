import { NgModule}      from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {PiwikService}          from '../openaireLibrary/utils/piwik/piwik.service';

import {LearnHowComponent} from "./learn-how.component";
import {LearnHowRoutingModule} from "./learn-how-routing.module";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {HtmlPagesModule} from "../htmlPages/htmlPages.module";
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {TabsModule} from '../openaireLibrary/utils/tabs/tabs.module';

@NgModule({
  imports: [
    CommonModule, RouterModule, LearnHowRoutingModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, BreadcrumbsModule, HtmlPagesModule, IconsModule, TabsModule
  ],
  declarations: [
    LearnHowComponent
  ],
  exports: [
    LearnHowComponent
  ],
  providers:[
    PreviousRouteRecorder, PiwikService, IsRouteEnabled
  ]
})
export class LearnHowModule {
	constructor(private iconsService: IconsService) {
		
	}
}
