import {Component} from "@angular/core";
import {properties} from "../../environments/environment";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {SearchCustomFilter} from "../openaireLibrary/searchPages/searchUtils/searchUtils.class";
import {Router} from "@angular/router";
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'community-sdg',
  template: `
    <sdg  *ngIf="pageContents" [customFilter]="customFilter" [pageContents]="pageContents"></sdg>
  `
})
export class CommunitySdgComponent {
  communityId;
  customFilter: SearchCustomFilter = null;
  public pageContents = null;
  subs: Subscription[] = [];
  constructor(
              private _router: Router,
              private helper: HelperService) {
  }

  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
    this.customFilter = new SearchCustomFilter("Community", "communityId", this.communityId, "");
    this.getPageContents();
  }
  ngOnDestroy() {
    for (let sub of this.subs) {
      sub.unsubscribe();
    }
  }

  private getPageContents() {
    this.subs.push(this.helper.getPageHelpContents(properties, this.communityId, this._router.url).subscribe(contents => {
      this.pageContents = contents;
      console.log(this.pageContents)
    }));
  }
}
