import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {CommunitySdgComponent} from "./sdg.component";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: CommunitySdgComponent,
        canActivate: [IsRouteEnabled],
        canDeactivate: [PreviousRouteRecorder]
      }
    ])
  ]
})
export class LibSdgRoutingModule {
}