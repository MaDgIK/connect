import {Component} from '@angular/core';
import {ActivatedRoute, Router}       from '@angular/router';

import {ConnectHelper} from '../openaireLibrary/connect/connectHelper';
import {properties} from "../../environments/environment";
import {Subscriber} from "rxjs";

@Component({
  selector: 'community-wrapper',
  template:`
      <community *ngIf="dashboard && communityId" [communityId]=communityId></community>
      <communities *ngIf="dashboard!=null && !dashboard" ></communities>
  `
})

export class CommunityWrapperComponent {
  communityId:string;
  dashboard:boolean = null;
  properties;
  private sub;
  constructor (private route: ActivatedRoute) {
    this.properties = properties;
    this.sub = this.route.queryParams.subscribe(
    communityId => {
      this.communityId  = ConnectHelper.getCommunityFromDomain(this.properties.domain);
      if(!this.communityId) {
        this.communityId = communityId['communityId'];
      }

      if(this.communityId){
        this.dashboard = true;
      }else{
        this.dashboard = false;
      }

    });
  }

  public ngOnInit() {
  }

  ngOnDestroy() {
    if (this.sub instanceof Subscriber) {
      this.sub.unsubscribe();
    }
  }

}
