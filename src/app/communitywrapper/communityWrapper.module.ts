import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
 import{CommunityModule} from '../community/community.module';
 import{CommunitiesModule} from '../communities/communities.module';
import {CommunityWrapperComponent} from './communityWrapper.component';
import {CommunityWrapperRoutingModule} from './communityWrapper-routing.module';
@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
      CommunityWrapperRoutingModule, CommunityModule, CommunitiesModule
  ],
  declarations: [
    CommunityWrapperComponent
   ],
  providers:[
    PreviousRouteRecorder,

    ],
  exports: [
    CommunityWrapperComponent
     ]
})
export class CommunityWrapperModule { }
