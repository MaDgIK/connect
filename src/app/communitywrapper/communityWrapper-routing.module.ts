import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{CommunityWrapperComponent} from './communityWrapper.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: CommunityWrapperComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class CommunityWrapperRoutingModule { }
