import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OpenaireErrorPageComponent} from './error/errorPage.component';
import {PageURLResolverComponent} from "./openaireLibrary/utils/pageURLResolver.component";
import {CommunityAccessGuard} from "./utils/communityAccess.guard";

const routes: Routes = [
  // ORCID Pages
  {
    path: 'orcid',
    loadChildren: () => import('./orcid/orcid.module').then(m => m.LibOrcidModule),
    data: {hasQuickContact: false}
  },
  {
    path: 'my-orcid-links',
    loadChildren: () => import('./orcid/my-orcid-links/myOrcidLinks.module').then(m => m.LibMyOrcidLinksModule),
    data: {hasQuickContact: false}
  },

  /** Other Pages */
  {
    path: '',
    loadChildren: () => import('./communitywrapper/communityWrapper.module').then(m => m.CommunityWrapperModule),
    data: {hasStickyHeaderOnMobile: true}
  },
  {path: 'about', redirectTo: 'about/learn-how', pathMatch: 'full'},
  {path: 'about/learn-how', loadChildren: () => import('./learn-how/learn-how.module').then(m => m.LearnHowModule), data: {hasStickyHeaderOnMobile: true}},
  {path: 'subscriptions', loadChildren: () => import('./pricing/pricing.module').then(m => m.PricingModule)},
  {path: 'get-started', loadChildren: () => import('./get-started/get-started.module').then(m => m.GetStartedModule)},
  {
    path: 'contact-us',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule),
    data: {hasQuickContact: false}
  },
  {
    path: 'invite',
    loadChildren: () => import('./utils/subscribe/invite/invite.module').then(m => m.InviteModule),
    data: {hasQuickContact: false}
  },
  {
    path: 'content',
    loadChildren: () => import('./content/contentPage.module').then(m => m.ContentPageModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'sdgs',
    loadChildren: () => import('./sdg/sdg.module').then(m => m.LibSdgModule),
    data: {hasQuickContact: false}
  },
  {path: 'fields-of-science', loadChildren: () => import('./openaireLibrary/fos/fos.module').then(m => m.FosModule), data: {extraOffset: 100, hasQuickContact: false}},
  {
    path: 'organizations',
    loadChildren: () => import('./htmlPages/organizations/organizationsPage.module').then(m => m.OrganizationsPageModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'publications',
    loadChildren: () => import('./htmlPages/publications/publications-page.module').then(m => m.PublicationsPageModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'projects',
    loadChildren: () => import('./htmlPages/projects/projectsPage.module').then(m => m.ProjectsPageModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'national-bulletins',
    loadChildren: () => import('./htmlPages/nationalBulletins/nationalBulletinsPage.module').then(m => m.NaionalBulletinPageModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'faqs',
    loadChildren: () => import('./htmlPages/featured/featuredPage.module').then(m => m.FeaturedPageModule),
    data: {hasQuickContact: false}
  },
  {
    path: 'roadmap',
    loadChildren: () => import('./htmlPages/featured/featuredPage.module').then(m => m.FeaturedPageModule),
    data: {hasQuickContact: false}
  },
  // {
  //   path: 'featured',
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: () => import('./htmlPages/featured/featuredPage.module').then(m => m.FeaturedPageModule),
  //       data: {hasQuickContact: false}, canActivateChild: [CommunityAccessGuard],
  //     },
  //     {
  //       path: '**',
  //       loadChildren: () => import('./htmlPages/featured/featuredPage.module').then(m => m.FeaturedPageModule),
  //       data: {hasQuickContact: false}, canActivateChild: [CommunityAccessGuard],
  //     },
  //   ]
  // },
  {
    path: 'curators',
    loadChildren: () => import('./openaireLibrary/connect/components/curators/curators.module').then(m => m.CuratorsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'subjects',
    loadChildren: () => import('./subjects/subjects.module').then(m => m.SubjectsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'myCommunities',
    loadChildren: () => import('./my-communities/my-communities.module').then(m => m.MyCommunitiesModule)
  },
  {
    path: 'develop',
    loadChildren: () => import('./develop/develop.module').then(m => m.DevelopModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  /** Testing Page for help contents */
  {path: 'helper-test', loadChildren: () => import('./helper-test/helper-test.module').then(m => m.HelperTestModule)},
  /**  Landing Pages */
  {
    path: 'search/result',
    loadChildren: () => import('./landingPages/result/libResult.module').then(m => m.LibResultModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/publication',
    loadChildren: () => import('./landingPages/publication/libPublication.module').then(m => m.LibPublicationModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/dataset',
    loadChildren: () => import('./landingPages/dataset/libDataset.module').then(m => m.LibDatasetModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/software',
    loadChildren: () => import('./landingPages/software/libSoftware.module').then(m => m.LibSoftwareModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/other', loadChildren: () => import('./landingPages/orp/libOrp.module').then(m => m.LibOrpModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/project',
    loadChildren: () => import('./landingPages/project/libProject.module').then(m => m.LibProjectModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/dataprovider',
    loadChildren: () => import('././landingPages/dataProvider/libDataProvider.module').then(m => m.LibDataProviderModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/organization',
    loadChildren: () => import('./landingPages/organization/libOrganization.module').then(m => m.LibOrganizationModule),
    data: {hasQuickContact: false, hasMenuSearchBar: true}, canActivate: [CommunityAccessGuard]
  },
  /**  Search Pages */
  {path: 'search/find', redirectTo: 'search/find/research-outcomes', pathMatch: 'full'},

  {
    path: 'search/find/communities',
    loadChildren: () => import('./searchPages/communities/searchCommunities.module').then(m => m.SearchCommunitiesModule)
  },
  {
    path: 'search/find/research-outcomes',
    loadChildren: () => import('./searchPages/simple/searchResearchResults.module').then(m => m.OpenaireSearchResearchResultsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/publications',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/datasets',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/software',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/other',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/projects',
    loadChildren: () => import('./searchPages/simple/searchProjects.module').then(m => m.LibSearchProjectsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/find/dataproviders',
    loadChildren: () => import('./searchPages/simple/searchDataProviders.module').then(m => m.LibSearchDataProvidersModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  /**  Advanced Search Pages */
  {
    path: 'search/advanced/research-outcomes',
    loadChildren: () => import('./searchPages/advanced/searchResearchResults.module').then(m => m.OpenaireAdvancedSearchResearchResultsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/publications',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/datasets',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/software',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/other',
    component: PageURLResolverComponent,
    data: {hasQuickContact: false},
    canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/organizations',
    loadChildren: () => import('./searchPages/advanced/advancedSearchOrganizations.module').then(m => m.LibAdvancedSearchOrganizationsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/dataproviders',
    loadChildren: () => import('./searchPages/advanced/advancedSearchDataProviders.module').then(m => m.LibAdvancedSearchDataProvidersModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'search/advanced/projects',
    loadChildren: () => import('./searchPages/advanced/advancedSearchProjects.module').then(m => m.LibAdvancedSearchProjectsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  /** Deposit Pages */
  { path: 'participate/deposit-datasets',
    redirectTo: 'participate/deposit/learn-how',
    pathMatch: 'full' },
  { path: 'participate/deposit-datasets-result',
    redirectTo: 'participate/deposit/learn-how',
    pathMatch: 'full' },
  { path: 'participate/deposit-subject-result',
    redirectTo: 'participate/deposit/learn-how',
    pathMatch: 'full' },
  { path: 'participate/deposit-publications',
    redirectTo: 'participate/deposit/learn-how',
    pathMatch: 'full' },
  { path: 'participate/deposit-publications-result',
    redirectTo: 'participate/deposit/learn-how',
    pathMatch: 'full' },
  { path: 'participate/share-zenodo',
    redirectTo: '/participate/deposit/zenodo',
    pathMatch: 'full' },
  {
    path: 'participate/deposit/learn-how',
    loadChildren: () => import('./deposit/deposit.module').then(m => m.LibDepositModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'participate/deposit/search',
    loadChildren: () => import('./deposit/searchDataprovidersToDeposit.module').then(m => m.LibSearchDataprovidersToDepositModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'participate/deposit/zenodo',
    loadChildren: () => import('./deposit/zenodo/shareInZenodo.module').then(m => m.ShareInZenodoModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'participate/deposit/suggested',
    loadChildren: () => import('./deposit/suggested/suggestedRepositories.module').then(m => m.SuggestedRepositoriesModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  /**  Linking Pages */
  {
    path: 'myclaims', loadChildren: () => import('./claims/myClaims/myClaims.module').then(m => m.LibMyClaimsModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'participate/claim',
    loadChildren: () => import('./claims/linking/linkingGeneric.module').then(m => m.LibLinkingGenericModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'participate/direct-claim',
    loadChildren: () => import('./claims/directLinking/directLinking.module').then(m => m.LibDirectLinkingModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  {
    path: 'preview',
    loadChildren: () => import('./demo/demo.module').then(m => m.DemoModule),
    data: {hasQuickContact: false}, canActivate: [CommunityAccessGuard]
  },
  /**  help pages - do not exist in Admin portal/api/db */
  {
    path: 'reload',
    loadChildren: () => import('./reload/libReload.module').then(m => m.LibReloadModule),
    data: {hasQuickContact: false}
  },
  {
    path: 'user-info',
    loadChildren: () => import('./login/libUser.module').then(m => m.LibUserModule),
    data: {hasQuickContact: false}
  },
  {path: 'error', component: OpenaireErrorPageComponent, data: {hasQuickContact: false}},
  {
    path: 'theme',
    loadChildren: () => import('./openaireLibrary/utils/theme/theme.module').then(m => m.ThemeModule),
    data: {hasQuickContact: false}
  },
  {path: '**', pathMatch: 'full', component: OpenaireErrorPageComponent, data: {hasQuickContact: false}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: "reload"
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
