import {Component} from '@angular/core';
import {Breadcrumb} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";

@Component({
  selector: 'content',
  template: `
    <div class="uk-container uk-container-large uk-section uk-section-small uk-padding-remove-bottom">
      <div class="uk-padding-small uk-padding-remove-horizontal">
        <breadcrumbs [breadcrumbs]="breadcrumbs"></breadcrumbs>
      </div>
      <html-page description="Sources and methodology" pageTitle="Sources and methodology"></html-page>
    </div>
  `
})
export class ContentPageComponent {
  public breadcrumbs: Breadcrumb[] = [{name: 'home', route: '/'}, {name: 'about - Sources and Methodology'}];
}
