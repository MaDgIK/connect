import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {ContentPageComponent} from './contentPage.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: ContentPageComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class ContentPageRoutingModule { }
