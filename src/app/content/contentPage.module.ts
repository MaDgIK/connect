import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

import {ContentPageComponent} from './contentPage.component';
import {ContentPageRoutingModule} from './content-routing.module';
import {CommonModule} from "@angular/common";
import {HtmlPagesModule} from "../htmlPages/htmlPages.module";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";


@NgModule({
  imports: [
    ContentPageRoutingModule, RouterModule, CommonModule,
    HtmlPagesModule, BreadcrumbsModule
  ],
  declarations: [
    ContentPageComponent
  ],
  providers: [ PreviousRouteRecorder, IsRouteEnabled],
  exports: [
    ContentPageComponent
  ]
})


export class ContentPageModule {
}
