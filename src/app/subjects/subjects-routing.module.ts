import { NgModule}      from '@angular/core';
import { RouterModule } from '@angular/router';
import {SubjectsComponent} from "./subjects.component";
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: SubjectsComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class SubjectsRoutingModule {
}
