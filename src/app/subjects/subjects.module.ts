import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }  from '@angular/forms';
import { RouterModule } from '@angular/router';

import {SubjectsComponent} from './subjects.component';
import {SubjectsRoutingModule} from "./subjects-routing.module";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {PiwikServiceModule} from "../openaireLibrary/utils/piwik/piwikService.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SubjectsRoutingModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, PiwikServiceModule,
    BreadcrumbsModule
  ],
  declarations: [
    SubjectsComponent
  ],
  exports: [
    SubjectsComponent
  ]
})
export class SubjectsModule {}
