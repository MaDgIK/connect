import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {OpenaireOrcidComponent} from './orcid.component';
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OpenaireOrcidComponent,
        canActivate: [LoginGuard], data: {
          redirect: '/error',  community : 'openaire'
        },
        canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class OrcidRoutingModule { }
