import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';

import {OpenaireOrcidComponent} from './orcid.component';
import {OrcidRoutingModule} from './orcid-routing.module';
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {FormsModule} from "@angular/forms";
import {OrcidModule} from "../openaireLibrary/orcid/orcid.module";
import {LoginGuard} from "../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    OrcidModule, OrcidRoutingModule
  ],
  declarations: [
    OpenaireOrcidComponent
  ],
  providers: [PreviousRouteRecorder, LoginGuard],
  exports: [
    OpenaireOrcidComponent
  ]
})
export class LibOrcidModule { }
