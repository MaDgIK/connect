import { Component } from '@angular/core';

@Component({
  selector: 'openaire-orcid',
  template: `
    <orcid></orcid>
  `
})

export class OpenaireOrcidComponent {}