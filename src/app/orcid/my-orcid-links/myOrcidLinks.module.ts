import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import {PreviousRouteRecorder} from "../../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {OpenaireMyOrcidLinksComponent} from './myOrcidLinks.component';
import {MyOrcidLinksModule} from "../../openaireLibrary/orcid/my-orcid-links/myOrcidLinks.module";
import {MyOrcidLinksRoutingModule} from "./myOrcidLinks-routing.module";
import {LoginGuard} from "../../openaireLibrary/login/loginGuard.guard";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    MyOrcidLinksModule,
    MyOrcidLinksRoutingModule
  ],
  declarations: [
    OpenaireMyOrcidLinksComponent
  ],
  exports: [
    OpenaireMyOrcidLinksComponent
  ],
  providers:    [PreviousRouteRecorder, LoginGuard]
})
export class LibMyOrcidLinksModule { }
