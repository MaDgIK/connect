import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Subscriber, Subscription} from "rxjs";
import {EnvProperties} from "../../openaireLibrary/utils/properties/env-properties";
import {properties} from "../../../environments/environment";
import {CommunityService} from "../../openaireLibrary/connect/community/community.service";

@Component({
  selector: 'openaire-my-orcid-links',
  template: `
    <my-orcid-links [communityId]="communityId"></my-orcid-links>   
  `
})

export class OpenaireMyOrcidLinksComponent {
  properties: EnvProperties = properties;
  public communityId = null;

  subs: Subscription[] = [];

  constructor(private  route: ActivatedRoute, private _communityService: CommunityService) {}

  public ngOnInit() {
    this.subs.push(this._communityService.getCommunityAsObservable().subscribe(
      community => {
        if(community) {
          this.communityId = community.communityId;
        }
      }));
  }

  public ngOnDestroy() {
    for (let sub of this.subs) {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    }
  }
}

