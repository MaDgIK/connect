import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {StatisticsComponent} from './statistics.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: StatisticsComponent, canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class StatisticsRoutingModule { }
