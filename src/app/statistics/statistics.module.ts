import {NgModule}                    from '@angular/core';
import {CommonModule, TitleCasePipe} from '@angular/common';
import {FormsModule}                 from '@angular/forms';
import {RouterModule}                from '@angular/router';

import {StatisticsRoutingModule}     from './statistics-routing.module';
import {IFrameModule}                from '../openaireLibrary/utils/iframe.module';

import {StatisticsComponent, StatisticsForDashboardComponent} from './statistics.component';

import {StatisticsService}           from '../utils/services/statistics.service';
import {PiwikService}                from '../openaireLibrary/utils/piwik/piwik.service';

import {PreviousRouteRecorder}       from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../openaireLibrary/error/isRouteEnabled.guard'

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    StatisticsRoutingModule, IFrameModule
  ],
  declarations: [
    StatisticsComponent,
    StatisticsForDashboardComponent
  ],
  providers: [
    IsRouteEnabled,  PreviousRouteRecorder,
    PiwikService, StatisticsService, TitleCasePipe
  ],
  exports: [
    StatisticsForDashboardComponent
  ]
})
export class StatisticsModule { }
