import { NgModule}      from '@angular/core';
import { RouterModule } from '@angular/router';
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {DevelopComponent} from "./develop.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: DevelopComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder]}
    ])
  ]
})
export class DevelopRoutingModule {
}
