import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DevelopComponent} from "./develop.component";
import {RouterModule} from "@angular/router";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";
import {PageContentModule} from "../openaireLibrary/dashboard/sharedComponents/page-content/page-content.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {DevelopRoutingModule} from "./develop-routing.module";

@NgModule({
  declarations: [DevelopComponent],
  imports: [CommonModule, RouterModule.forChild([
    {
      path: '',
      component: DevelopComponent,
      canDeactivate: [PreviousRouteRecorder]
    },
  ]), PageContentModule, IconsModule, DevelopRoutingModule],
  exports: [DevelopComponent]
})
export class DevelopModule {

}
