import {Component, OnDestroy, OnInit} from "@angular/core";
import {CommunityInfo} from "../openaireLibrary/connect/community/communityInfo";
import {CommunityService} from "../openaireLibrary/connect/community/community.service";
import {Subscription} from "rxjs";
import {Meta, Title} from "@angular/platform-browser";
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {properties} from "../../environments/environment";
import {Router} from "@angular/router";
import {OpenaireEntities} from "../openaireLibrary/utils/properties/searchFields";
import {EnvProperties} from "../openaireLibrary/utils/properties/env-properties";

@Component({
  selector: 'develop',
  template: `
      <div class="uk-section">
				<div class="uk-container uk-container-large">
						<h1>OpenAIRE APIs<br> for developers<span class="uk-text-primary">.</span></h1>
				</div>
				<div class="uk-section uk-container uk-container-large">
					<div class="uk-grid uk-grid-large uk-child-width-1-2@m" uk-grid>
						<div class="uk-text-center uk-margin-large-top">
							<div class="uk-width-2-3@m uk-margin-auto@m">
								<div class="uk-icon-bg-shadow uk-icon-bg-shadow-large uk-margin-auto">
									<icon name="description" customClass="uk-text-background" [flex]="true" ratio="2.5" type="outlined" visuallyHidden="For {{openaireEntities.RESULTS}}"></icon>
								</div>
								<h3>For {{openaireEntities.RESULTS | lowercase}}</h3>
								<div class="uk-margin-bottom">
									For {{openaireEntities.RESULTS | lowercase}} ({{openaireEntities.PUBLICATIONS | lowercase}}, {{openaireEntities.DATASETS | lowercase}}, {{openaireEntities.SOFTWARE | lowercase}} and {{openaireEntities.OTHER | lowercase}}) you can use the Graph API by adding the community parameter.
								</div>
								<a class="uk-display-inline-block uk-button uk-button-text"
										href="https://graph.openaire.eu/docs/apis/search-api/research-products" target="_blank">
									<span class="uk-flex uk-flex-middle">
										<span>Graph APIs</span>
									</span>
								</a>
							</div>
						</div>
						<div class="uk-margin-large-top">
<!--							<img src="assets/common-assets/apis.svg" alt="APIs" loading="lazy">-->
              <div class="uk-margin-top">
                <div>Request examples:</div>
                <ul class="uk-list uk-list-large uk-list-bullet uk-list-primary">
                  <li>
                    <span>Access all </span><span class="uk-text-bolder">{{openaireEntities.RESULTS}}</span> ({{openaireEntities.PUBLICATIONS}}, {{openaireEntities.DATASETS}}, {{openaireEntities.SOFTWARE}}, {{openaireEntities.OTHER}})<br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="">https://api.openaire.eu/search/researchProducts?community={{community.communityId}}</span>
                  </li>
                  <li>
                    <span>Access </span><span class="uk-text-bolder">{{openaireEntities.PUBLICATIONS}}</span><br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="">https://api.openaire.eu/search/publications?community={{community.communityId}}</span>
                  </li>
                  <li>
                    <span>Access </span> <span class="uk-text-bolder">Open Access {{openaireEntities.PUBLICATIONS}}</span><br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="uk-text-break">http://api.openaire.eu/search/publications?community={{community.communityId}}&OA=true</span>
                  </li>
                  <li>
                    <span>Access </span><span class="uk-text-bolder">{{openaireEntities.DATASETS}}</span><br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="uk-text-break">https://api.openaire.eu/search/datasets?community={{community.communityId}}</span>
                  </li>
                  <li>
                    <span>Access </span><span class="uk-text-bolder">{{openaireEntities.SOFTWARE}}</span><br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="uk-text-break">https://api.openaire.eu/search/software?community={{community.communityId}}</span>
                  </li>
                  <li>
                    <span>Access </span><span class="uk-text-bolder">{{openaireEntities.OTHER}}</span><br>
                    <span class="uk-text-bold uk-margin-small-right">GET</span>
                    <span class="uk-text-break">https://api.openaire.eu/search/other?community={{community.communityId}}</span>
                  </li>
                </ul>
              </div>
						</div>
					</div>
				</div>
        <div class="uk-container uk-container-large uk-text-center">
          <h1>Download records<span class="uk-text-primary">.</span></h1>
          <div class="uk-width-1-1 uk-flex uk-flex-center">
            <div class="uk-width-large">
              To get all metadata records of the community in bulk, use the dump of research communities on Zenodo.
              It contains one tar file per community.
              Each tar file contains gzipped tar files with one JSON per line.
              <br>
              <a href="https://graph.openaire.eu/docs/downloads/subgraphs#the-dumps-about-research-communities-initiatives-and-infrastructures" target="_blank">
                Learn more <span uk-icon="icon:chevron-right; ratio:0.7"></span></a>
            </div>
          </div>
        </div>
         
        <div class="uk-section uk-container uk-container-small">
          <div class="uk-grid uk-grid-large uk-flex uk-flex-center" uk-grid>
            <div class="uk-flex uk-flex-middle uk-flex-center uk-card uk-card-default uk-padding uk-width-medium">
              <a class="uk-display-inline-block uk-button uk-button-text"
                 href="https://doi.org/10.5281/zenodo.3974604" target="_blank">
                    <span class="uk-flex uk-flex-middle">
                      <span>Community dump</span>
                    </span>
              </a>
              
            </div>
            <div class="uk-flex uk-flex-middle uk-flex-center uk-card uk-card-default uk-padding uk-width-medium uk-margin-large-left">
              
              <a class="uk-display-inline-block uk-button uk-button-text"
                 href="https://doi.org/10.5281/zenodo.3974225" target="_blank">
                    <span class="uk-flex uk-flex-middle">
                      <span>Schema</span>
                    </span>
              </a>
            </div>
          </div>
             
          </div>
       </div>
  `
})
export class DevelopComponent implements OnInit, OnDestroy {
  
  public community: CommunityInfo;
  public openaireEntities = OpenaireEntities;
  public properties: EnvProperties = properties;

  private subscriptions: any[] = [];
  
  constructor(private communityService: CommunityService,
              private seoService: SEOService,
              private _meta: Meta,
              private _router: Router,
              private _title: Title) {
  }
  
  ngOnInit() {
    this.subscriptions.push(this.communityService.getCommunityAsObservable().subscribe(community => {
      this.community = community;
      if (this.community) {
        /* Metadata */
        const url = properties.domain + properties.baseLink + this._router.url;
        this.seoService.createLinkForCanonicalURL(url, false);
        this._meta.updateTag({content: url}, "property='og:url'");
        const description = "Develop | " + this.community.shortTitle;
        const title = "Develop | " + this.community.shortTitle;
        this._meta.updateTag({content: description}, "name='description'");
        this._meta.updateTag({content: description}, "property='og:description'");
        this._meta.updateTag({content: title}, "property='og:title'");
        this._title.setTitle(title);
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscription) {
        subscription.unsubscribe();
      }
    });
  }
}
