import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {ShareInZenodoComponent} from './shareInZenodo.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: ShareInZenodoComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class ShareInZenodoRoutingModule { }
