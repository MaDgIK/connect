import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';

import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../../openaireLibrary/error/isRouteEnabled.guard'

import {ZenodoCommunitiesServiceModule} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communitiesService.module';

import {RouterModule} from "@angular/router";
import {HelperModule} from "../../openaireLibrary/utils/helper/helper.module";
import {PiwikServiceModule} from "../../openaireLibrary/utils/piwik/piwikService.module";
import {Schema2jsonldModule} from "../../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {BreadcrumbsModule} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {NoLoadPaging} from "../../openaireLibrary/searchPages/searchUtils/no-load-paging.module";
import {IconsModule} from 'src/app/openaireLibrary/utils/icons/icons.module';
import {LoadingModule} from 'src/app/openaireLibrary/utils/loading/loading.module';
import {SuggestedRepositoriesRoutingModule} from "./suggestedRepositories-routing.module";
import {SuggestedRepositoriesComponent} from "./suggestedRepositories.component";
import {SearchCommunityDataprovidersService} from "../../openaireLibrary/connect/contentProviders/searchDataproviders.service";
import {IsPageEnabledModule} from "../../openaireLibrary/utils/isPageEnabled/isPageEnabled.module";

@NgModule({
  imports: [
    CommonModule, SuggestedRepositoriesRoutingModule, ZenodoCommunitiesServiceModule,
    RouterModule, HelperModule, LoadingModule,
    PiwikServiceModule, Schema2jsonldModule, SEOServiceModule, BreadcrumbsModule, NoLoadPaging, IconsModule, IsPageEnabledModule
  ],
  declarations: [
    SuggestedRepositoriesComponent
  ],
  providers:[PreviousRouteRecorder, IsRouteEnabled, SearchCommunityDataprovidersService],
  exports: [
    SuggestedRepositoriesComponent
  ]
})


export class SuggestedRepositoriesModule {}
