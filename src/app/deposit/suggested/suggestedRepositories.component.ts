import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ZenodoCommunitiesService} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';
import {CommunityService} from '../../openaireLibrary/connect/community/community.service';
import {CommunityInfo} from '../../openaireLibrary/connect/community/communityInfo';
import {SearchUtilsClass} from '../../openaireLibrary/searchPages/searchUtils/searchUtils.class';
import {ErrorCodes} from '../../openaireLibrary/utils/properties/errorCodes';
import {HelperService} from "../../openaireLibrary/utils/helper/helper.service";
import {RouterHelper} from "../../openaireLibrary/utils/routerHelper.class";
import {SEOService} from "../../openaireLibrary/sharedComponents/SEO/SEO.service";
import {PiwikService} from "../../openaireLibrary/utils/piwik/piwik.service";
import {Breadcrumb} from "../../openaireLibrary/utils/breadcrumbs/breadcrumbs.component";
import {properties} from "../../../environments/environment";
import {Subscriber, Subscription} from "rxjs";
import {SearchCommunityDataprovidersService} from "../../openaireLibrary/connect/contentProviders/searchDataproviders.service";

@Component({
  selector: 'suggested-repositories',
  templateUrl: './suggestedRepositories.component.html'
})
export class SuggestedRepositoriesComponent {
  public url: string = null;
  public title: string = "Suggested Repositories";
  properties: EnvProperties = properties;
  communityId: string = null;
  community: CommunityInfo = null;
  public pageContents = null;
  public divContents = null;
  
  
  // public warningMessage = "";
  // public infoMessage = "";
  
  masterZenodoCommunityId = null;
  masterZenodoCommunity = null;
  
  communityIds = null;
  communities = [];
  page = 1;
  size = 5;
  zenodoCommunitiesLoadedCount = 0;
  zenodoSearchUtils: SearchUtilsClass = new SearchUtilsClass();
  errorCodes: ErrorCodes = new ErrorCodes();
  depositLink = "https://zenodo.org/deposit/new?c=";
  depositLearnHowPage: string = null;
  public routerHelper: RouterHelper = new RouterHelper();
  breadcrumbs: Breadcrumb[] = [];
  
  subs: Subscription[] = [];
  contentProviders;

  constructor(private  route: ActivatedRoute,
              private _router: Router,
              private _meta: Meta,
              private _title: Title,
              private _zenodoCommunitieService: ZenodoCommunitiesService,
              private _communityService: CommunityService,
              private searchCommunityDataprovidersService: SearchCommunityDataprovidersService,
              private helper: HelperService,
              private _piwikService: PiwikService,
              private seoService: SEOService) {
  }
  
  public ngOnInit() {
    this.zenodoSearchUtils.status = this.errorCodes.LOADING;
    this.url = properties.domain + properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(this.url, false);
    this.updateUrl(this.url);
    this.updateTitle(this.title);
    this.updateDescription("sugested repositories, deposit");
    this.depositLearnHowPage = this.properties.depositLearnHowPage;
    this.breadcrumbs.push({name: 'home', route: '/'}, {
      name: "Deposit",
      route: this.depositLearnHowPage
    }, {name: "Suggested Repositories", route: null});
    //this.getDivContents();
    this.getPageContents();
    this.subs.push(this._communityService.getCommunityAsObservable().subscribe(
      community => {
        if (community) {
          this.communityId = community.communityId;
          this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
          this.community = community;
          this.masterZenodoCommunityId = this.community.zenodoCommunity;
          this.subs.push(this.searchCommunityDataprovidersService.searchDataproviders(this.properties, this.community.communityId, true).subscribe(
            res => {
              this.contentProviders = res;
              this.zenodoSearchUtils.status = this.contentProviders && this.contentProviders.length > 0 ? this.errorCodes.DONE: this.errorCodes.NONE;
            },
            error => {
              console.log(error);
            }
          ));

        }
      }));
    
  }
  
  public ngOnDestroy() {
    for (let sub of this.subs) {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    }
  }
  
  private updateDescription(description: string) {
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
  }
  
  private updateTitle(title: string) {
    var _title = ((title.length > 50) ? title.substring(0, 50) : title);
    this._title.setTitle(_title);
    this._meta.updateTag({content: _title}, "property='og:title'");
  }
  
  private updateUrl(url: string) {
    this._meta.updateTag({content: url}, "property='og:url'");
  }
  
  private getPageContents() {
    this.subs.push(this.helper.getPageHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  private getDivContents() {
    this.subs.push(this.helper.getDivHelpContents(this.properties, this.communityId, this._router.url).subscribe(contents => {
      this.divContents = contents;
    }));
  }

  private handleError(message: string, error) {
    console.error("Share in Zenodo Page: " + message, error);
  }

}
