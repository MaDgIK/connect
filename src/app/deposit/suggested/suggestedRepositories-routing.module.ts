import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from "../../openaireLibrary/error/isRouteEnabled.guard";
import {SuggestedRepositoriesComponent} from "./suggestedRepositories.component";

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: SuggestedRepositoriesComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class SuggestedRepositoriesRoutingModule { }
