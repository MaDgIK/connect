import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';

import {ZenodoCommunitiesService} from '../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';
import {CommunityService} from '../openaireLibrary/connect/community/community.service';

import {ZenodoInformationClass} from '../openaireLibrary/deposit/utils/zenodoInformation.class';
import {FetchZenodoInformation} from './utils/fetchZenodoInformation.class';
import {Subscriber, Subscription} from "rxjs";
import {properties} from "../../environments/environment";
import {SearchCommunityDataprovidersService} from "../openaireLibrary/connect/contentProviders/searchDataproviders.service";

@Component({
  selector: 'openaire-search-deposit',
  template: `
    <deposit-search-dataproviders [zenodoInformation]="zenodoInformation"
                                  [communityId]="communityId" [searchForm]="{dark: false, class: 'search-form'}"></deposit-search-dataproviders>
  `
})

export class OpenaireSearchDataprovidersToDepositComponent {
  public zenodoInformation: ZenodoInformationClass = new ZenodoInformationClass();
  properties: EnvProperties = properties;
  fetchZenodoInformation: FetchZenodoInformation;
  communityId: string = null;
  
  subs: Subscription[] = [];
  
  constructor(private  route: ActivatedRoute,
              private _zenodoCommunitieService: ZenodoCommunitiesService,
              private _communityService: CommunityService,
              private searchCommunityDataprovidersService: SearchCommunityDataprovidersService) {
    this.fetchZenodoInformation = new FetchZenodoInformation(this._zenodoCommunitieService);
  }
  
  public ngOnInit() {
    this.subs.push(this._communityService.getCommunityAsObservable().subscribe(
      community => {
        if(community) {
          this.communityId = community.communityId;
          let masterZenodoCommunityId = community.zenodoCommunity;
          if (masterZenodoCommunityId  || (community.otherZenodoCommunities && community.otherZenodoCommunities.length > 0)) {
            this.zenodoInformation.shareInZenodoUrl = this.properties.shareInZenodoPage;
          } else {
            this.zenodoInformation.url = this.properties.zenodo;
            this.zenodoInformation.name = "Zenodo";
          }
          this.subs.push(this.searchCommunityDataprovidersService.searchDataproviders(this.properties, this.communityId, true).subscribe(
            res => {
              if(res && res.length > 0){
                this.zenodoInformation.hasSuggestedRepositories = true;
              }
            },
            error => {
              console.log(error);
            }
          ));
        }
      }
    ));
    
    if (!this.zenodoInformation.shareInZenodoUrl) {
      this.zenodoInformation.url = this.properties.zenodo;
    }
    if (!this.zenodoInformation.name) {
      this.zenodoInformation.name = "Zenodo";
    }
  }
  
  public ngOnDestroy() {
    for (let sub of this.subs) {
      if (sub instanceof Subscriber) {
        sub.unsubscribe();
      }
    }
    this.fetchZenodoInformation.clearSubscriptions();
  }
  
  private handleError(message: string, error) {
    console.error("Deposit Publications Page: " + message, error);
  }
}
