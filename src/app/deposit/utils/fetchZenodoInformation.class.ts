import {EnvProperties} from '../../openaireLibrary/utils/properties/env-properties';
import {ZenodoCommunitiesService} from '../../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service';

import {ZenodoInformationClass} from '../../openaireLibrary/deposit/utils/zenodoInformation.class';
import {Subscriber} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class FetchZenodoInformation {
  private subscriptions = [];
  constructor ( private _zenodoCommunitieService: ZenodoCommunitiesService ) { }

  public ngOnDestroy() {
    this.clearSubscriptions();
  }

  clearSubscriptions(){
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  public getZenodoCommunityNameAndUrlById(masterZenodoCommunityId: string, properties:EnvProperties, zenodoInformation: ZenodoInformationClass){
    this.subscriptions.push(this._zenodoCommunitieService.getZenodoCommunityById(properties, masterZenodoCommunityId).subscribe(
      result  => {
        console.info("getZenodoCommunityNameAndUrlById", result);
        var masterZenodoCommunity = result;
        //zenodoInformation.name = masterZenodoCommunity.title;
        zenodoInformation.url = masterZenodoCommunity.link;
      },
      error => {
        console.error("Master Zenodo community'"+masterZenodoCommunityId+"' couldn't be loaded");
      }
    ));
  }
}
