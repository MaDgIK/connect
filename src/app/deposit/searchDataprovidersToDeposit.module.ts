import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import { OpenaireSearchDataprovidersToDepositComponent } from './searchDataprovidersToDeposit.component';

import {SearchDataprovidersToDepositRoutingModule} from './searchDataprovidersToDeposit-routing.module';
import {SearchDataprovidersToDepositModule} from '../openaireLibrary/deposit/searchDataprovidersToDeposit.module';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {IsRouteEnabled} from '../openaireLibrary/error/isRouteEnabled.guard';
import {ZenodoCommunitiesServiceModule} from '../openaireLibrary/connect/zenodoCommunities/zenodo-communitiesService.module';
import {SearchCommunityDataprovidersService} from "../openaireLibrary/connect/contentProviders/searchDataproviders.service";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchDataprovidersToDepositModule,
    SearchDataprovidersToDepositRoutingModule,
    ZenodoCommunitiesServiceModule
  ],
  declarations: [
    OpenaireSearchDataprovidersToDepositComponent
  ],
  exports: [
    OpenaireSearchDataprovidersToDepositComponent,
  ],
  providers:    [PreviousRouteRecorder, IsRouteEnabled, SearchCommunityDataprovidersService]
})
export class LibSearchDataprovidersToDepositModule { }
