import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OpenaireProjectComponent } from './project.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenaireProjectComponent, data: {
        redirect: '/error' 
      },canDeactivate: [PreviousRouteRecorder]  }
  ])
 ]
})
export class ProjectRoutingModule { }
