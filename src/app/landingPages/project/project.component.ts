import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-project',
  template: `
    <project [communityId]="communityId"></project>`,
})
export class OpenaireProjectComponent implements OnInit {
  communityId;

  constructor() {
  }

  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
