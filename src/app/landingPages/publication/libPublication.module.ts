import { NgModule}            from '@angular/core';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import { OpenairePublicationComponent } from './publication.component';
import {PublicationRoutingModule} from './publication-routing.module';
import {ResultLandingModule} from "../../openaireLibrary/landingPages/result/resultLanding.module";

@NgModule({
  imports: [PublicationRoutingModule, ResultLandingModule],
  declarations:[OpenairePublicationComponent],
  providers:[ PreviousRouteRecorder],
  exports:[OpenairePublicationComponent]
})
export class LibPublicationModule { }
