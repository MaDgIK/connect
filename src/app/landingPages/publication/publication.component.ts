import {Component} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-publication',
  template: `
    <result-landing type="publication" [communityId]="communityId"></result-landing>
  `,
})
export class OpenairePublicationComponent {
  communityId = null;

  constructor() {}
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
