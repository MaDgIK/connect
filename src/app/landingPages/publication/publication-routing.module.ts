import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {OpenairePublicationComponent } from './publication.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenairePublicationComponent, data: {
        redirect: '/error' 
      },canDeactivate: [PreviousRouteRecorder]  }
  ])
]
})
export class PublicationRoutingModule { }
