import {Component} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-organization',
  template: `
    <organization [communityId]="communityId"></organization>`,
})
export class OpenaireOrganizationComponent {
  communityId;
  
  constructor() {
  }
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
