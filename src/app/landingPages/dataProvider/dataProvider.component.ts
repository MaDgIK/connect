import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-dataprovider',
  template: `
    <dataprovider [communityId]="communityId"></dataprovider>`,
})
export class OpenaireDataProviderComponent implements OnInit {
  communityId;
  
  constructor() {
  }

  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
