import { NgModule}            from '@angular/core';
import { DataProviderModule } from '../../openaireLibrary/landingPages/dataProvider/dataProvider.module';
import { OpenaireDataProviderComponent } from './dataProvider.component';
import {DataProviderRoutingModule} from './dataProvider-routing.module';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [DataProviderModule, DataProviderRoutingModule],
  declarations:[OpenaireDataProviderComponent],
  providers:[ PreviousRouteRecorder],
  exports:[OpenaireDataProviderComponent]
})
export class LibDataProviderModule { }
