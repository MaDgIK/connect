import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OpenaireDataProviderComponent } from './dataProvider.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenaireDataProviderComponent,  data: {
        redirect: '/error'
      },canDeactivate: [PreviousRouteRecorder] }
  ])
]
})
export class DataProviderRoutingModule { }
