import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OpenaireHtmlProjectReportComponent } from './htmlProjectReport.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenaireHtmlProjectReportComponent,
    data: {
        redirect: '/error' 
      },canDeactivate: [PreviousRouteRecorder]}
  ])
]
})
export class HtmlProjectReportRoutingModule { }
