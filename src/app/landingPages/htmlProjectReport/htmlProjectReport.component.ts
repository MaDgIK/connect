import {Component} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-htmlProjectReport',
  template: `
    <htmlProjectReport [communityId]="communityId"></htmlProjectReport>`,
})
export class OpenaireHtmlProjectReportComponent {
  communityId;
  
  constructor() {
  }
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
