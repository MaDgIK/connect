import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-orp',
  template: `
    <result-landing type="orp" [communityId]="communityId"></result-landing>
  `,
})

export class OpenaireOrpComponent implements OnInit {
  communityId = null;
  
  constructor() {}
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
