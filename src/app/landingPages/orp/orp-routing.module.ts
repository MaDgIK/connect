import {NgModule}               from '@angular/core';
import {RouterModule}           from '@angular/router';

import {OpenaireOrpComponent }  from './orp.component';
import {PreviousRouteRecorder}  from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([{
      path: '', component: OpenaireOrpComponent,  data: {
        redirect: '/error'
      },
      canDeactivate: [PreviousRouteRecorder]
    }])
  ]
})

export class OrpRoutingModule { }
