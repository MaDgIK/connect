import {NgModule}                    from '@angular/core';
import {PreviousRouteRecorder}       from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {OpenaireOrpComponent}        from './orp.component';
import {OrpRoutingModule}            from './orp-routing.module';
import {ResultLandingModule} from "../../openaireLibrary/landingPages/result/resultLanding.module";

@NgModule({
  imports: [
    OrpRoutingModule, ResultLandingModule
  ],
  declarations: [
    OpenaireOrpComponent
  ],
  providers: [
    PreviousRouteRecorder
  ],
  exports: [
    OpenaireOrpComponent
  ]
})
export class LibOrpModule { }
