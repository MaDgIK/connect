import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {OpenaireSoftwareComponent } from './software.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';


@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenaireSoftwareComponent, data: {
        redirect: '/error' 
      },canDeactivate: [PreviousRouteRecorder]  }
  ])
]
})
export class SoftwareRoutingModule { }
