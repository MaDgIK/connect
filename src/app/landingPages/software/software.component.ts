import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-software',
  template: `
    <result-landing type="software" [communityId]="communityId"></result-landing>
  `,
})
export class OpenaireSoftwareComponent implements OnInit {
  communityId;

  constructor() {
  }

  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
