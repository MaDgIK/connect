import { NgModule}            from '@angular/core';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import { OpenaireSoftwareComponent } from './software.component';
import {SoftwareRoutingModule} from './software-routing.module';
import {ResultLandingModule} from "../../openaireLibrary/landingPages/result/resultLanding.module";
@NgModule({
  imports: [SoftwareRoutingModule, ResultLandingModule],
  declarations:[OpenaireSoftwareComponent],
  providers:[ PreviousRouteRecorder],
  exports:[OpenaireSoftwareComponent]
})
export class LibSoftwareModule { }
