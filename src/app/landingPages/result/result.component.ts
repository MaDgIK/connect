import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from "../../openaireLibrary/connect/connectHelper";
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-publication',
  template: `
    <result-landing type="result" [communityId]="communityId"></result-landing>`,
})
export class OpenaireResultComponent implements OnInit{
  communityId;
  
  constructor() {}
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
