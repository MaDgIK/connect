import { NgModule}            from '@angular/core';
import { OpenaireDatasetComponent } from './dataset.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {DatasetRoutingModule} from './dataset-routing.module';
import {ResultLandingModule} from "../../openaireLibrary/landingPages/result/resultLanding.module";
 @NgModule({
   imports: [DatasetRoutingModule, ResultLandingModule],
  declarations:[OpenaireDatasetComponent],
  providers:[ PreviousRouteRecorder],
  exports:[OpenaireDatasetComponent]
})
export class LibDatasetModule { }
