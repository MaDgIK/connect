import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OpenaireDatasetComponent } from './dataset.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
 imports: [
  RouterModule.forChild([
    { path: '', component: OpenaireDatasetComponent, data: {
        redirect: '/error' 
      },canDeactivate: [PreviousRouteRecorder] }
  ])
]
})
export class DatasetRoutingModule { }
