import {Component, OnInit} from '@angular/core';
import {ConnectHelper} from '../../openaireLibrary/connect/connectHelper';
import {properties} from "../../../environments/environment";

@Component({
  selector: 'openaire-dataset',
  template: `
    <result-landing type="dataset" [communityId]="communityId"></result-landing>
  `,
})
export class OpenaireDatasetComponent implements OnInit {
  communityId;
  
  constructor() {
  }
  
  ngOnInit() {
    this.communityId = ConnectHelper.getCommunityFromDomain(properties.domain);
  }
}
