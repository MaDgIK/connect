import {Component, ElementRef, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {CommunityService} from "../openaireLibrary/connect/community/community.service";
import {ConfigurationService} from '../openaireLibrary/utils/configuration/configuration.service';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {User} from '../openaireLibrary/login/utils/helper.class';
import {SEOService} from "../openaireLibrary/sharedComponents/SEO/SEO.service";
import {UserManagementService} from "../openaireLibrary/services/user-management.service";
import {Subscription} from "rxjs";
import {properties} from "../../environments/environment";
import {ConnectHelper} from "../openaireLibrary/connect/connectHelper";
import {PluginsService} from "../openaireLibrary/services/plugins.service";
import {PluginTemplate} from "../openaireLibrary/utils/entities/adminTool/pluginTemplate";
import {Plugin} from "../openaireLibrary/utils/entities/adminTool/plugin";
import {PluginUtils} from "../openaireLibrary/dashboard/plugins/utils/pluginUtils";

@Component({
  selector: 'community',
  templateUrl: 'community.component.html',

})

export class CommunityComponent {
  public url: string = null;
  properties: EnvProperties = properties;
  @Input() communityId = null;
  public community = null;

  public portal = null;
  subscriptions: Subscription[] = [];
  private user: User;
  public showLoading = true;
  pluginTemplates: PluginTemplate[] = [];
  plugins: Plugin[] = [];
  public pluginsByPlacement: Map<string,{plugin:Plugin, template:PluginTemplate}[]> = new Map();
  public pluginUtils = new PluginUtils();

  constructor(
    private element: ElementRef,
    private route: ActivatedRoute,
    private _router: Router,
    private location: Location,
    private _meta: Meta,
    private _title: Title,
    private _piwikService: PiwikService,
    private config: ConfigurationService,
    private _communityService: CommunityService,

    private seoService: SEOService,
    private userManagementService: UserManagementService,
    private _pluginsService:PluginsService) {

    var description = "OpenAIRE - Connect, Community Dashboard, research community";
    var title = "OpenAIRE - Connect";
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
    this._meta.updateTag({content: title}, "property='og:title'");
    this._title.setTitle(title);

  }

  public ngOnInit() {
     this.url = properties.domain + properties.baseLink + this._router.url;
    this.seoService.createLinkForCanonicalURL(this.url, false);
    this._meta.updateTag({content: this.url}, "property='og:url'");

    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      this.subscriptions.push(this._communityService.getCommunityAsObservable().subscribe(
        community => {
          this.community = community;
          if(community && !ConnectHelper.isPrivate(community, this.user)) {

            this._meta.updateTag({content: community.description}, "name='description'");
            this._meta.updateTag({content: community.description}, "property='og:description'");
            this._meta.updateTag({content: community.displayTitle}, "property='og:title'");
            this._title.setTitle(community.displayTitle);
            this.subscriptions.push(this._piwikService.trackView(this.properties, community.displayTitle).subscribe());
            this.getPagePlugins();
          }
        }));
    }));

   }

  getPagePlugins() {
    this.showLoading = true;
    this.subscriptions.push(this._pluginsService.getPluginTemplatesByPageRoute(this.properties.adminToolsAPIURL, this.community.communityId,  this._router.url.split('?')[0] ).subscribe(
      templates => {
        this.pluginTemplates = templates;

        this.subscriptions.push(this._pluginsService.getPluginsByPageRoute(this.properties.adminToolsAPIURL, this.community.communityId, this._router.url.split('?')[0]).subscribe(
          plugins => {
            this.plugins = plugins;
            this.pluginsByPlacement = new Map();
            for(let pos of this.pluginUtils.placementsOptions){
              this.pluginsByPlacement.set(pos.value,[]);
            }
            let self = this;
            this.pluginTemplates.forEach(_ => {
              let plugin:Plugin = null;
                if(!_.custom) {
                  for (let pl of plugins) {
                    if (pl.templateId == _._id) {
                      plugin = pl;
                    }
                  }
                  if (!plugin) {
                    plugin = new Plugin("", "", _);
                    this.plugins.push(plugin);
                  }
                  plugin.object = PluginUtils.initializeObjectAndCompare(_.code, plugin.object)
                  this.pluginsByPlacement.get(plugin.placement).push({plugin: plugin, template: _});
                }
            });
            //add custom plugins in the list
            this.plugins.forEach(_ => {

              if(_.custom == true){
                let customTemplate = null;
                this.pluginTemplates.forEach(template => {
                  if (_.templateId == template._id) {
                    customTemplate = template;
                  }
                });
                if(customTemplate && customTemplate.custom){
                  this.pluginsByPlacement.get(customTemplate.placement).push({plugin: _, template: customTemplate});
                }
              }
            });

            for(let placement of this.pluginUtils.placementsOptions){
              this.pluginsByPlacement.get(placement.value).sort(function (a, b) {
                return a.plugin.order - b.plugin.order;
              })
            }
            this.showLoading = false;
          },
          error => {}));

      },
      error => {}));
  }
}
