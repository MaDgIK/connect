import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{CommunityComponent} from './community.component';
import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';

@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: CommunityComponent,canDeactivate: [PreviousRouteRecorder] }

    ])
  ]
})
export class CommunityRoutingModule { }
