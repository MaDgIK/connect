import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CommunityComponent} from './community.component';

import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';

import {PreviousRouteRecorder} from '../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {SubscribeModule} from '../utils/subscribe/subscribe.module';
import {InviteModule} from '../utils/subscribe/invite/invite.module';
import {ManageModule} from '../openaireLibrary/utils/manage/manage.module';

import {StatisticsModule} from "../statistics/statistics.module";
import {
  ZenodoCommunitiesServiceModule
} from '../openaireLibrary/connect/zenodoCommunities/zenodo-communitiesService.module';

import {SearchFormModule} from '../openaireLibrary/searchPages/searchUtils/searchForm.module';
import {
  SearchDataprovidersServiceModule
} from "../openaireLibrary/connect/contentProviders/searchDataprovidersService.module";
import {SearchProjectsServiceModule} from '../openaireLibrary/connect/projects/searchProjectsService.module';
import {SearchResearchResultsServiceModule} from "../openaireLibrary/services/searchResearchResultsService.module";
import {SearchResultsModule} from "../openaireLibrary/searchPages/searchUtils/searchResults.module";
import {CuratorsModule} from "../openaireLibrary/connect/components/curators/curators.module";
import {AffiliationsModule} from "../openaireLibrary/connect/affiliations/affiliations.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {MatSelectModule} from "@angular/material/select";
import {EntitiesSelectionModule} from "../openaireLibrary/searchPages/searchUtils/entitiesSelection.module";
import {TabsModule} from "../openaireLibrary/utils/tabs/tabs.module";
import {SearchTabModule} from "../openaireLibrary/utils/tabs/contents/search-tab.module";
import {ErrorMessagesModule} from "../openaireLibrary/utils/errorMessages.module";
import {SafeHtmlPipeModule} from '../openaireLibrary/utils/pipes/safeHTMLPipe.module';
import {ErrorModule} from "../openaireLibrary/error/error.module";
import {
  AdvancedSearchInputModule
} from '../openaireLibrary/sharedComponents/advanced-search-input/advanced-search-input.module';
import {InputModule} from '../openaireLibrary/sharedComponents/input/input.module';
import {QuickSelectionsModule} from '../openaireLibrary/searchPages/searchUtils/quick-selections.module';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {NoLoadPaging} from '../openaireLibrary/searchPages/searchUtils/no-load-paging.module';
import {NumberRoundModule} from '../openaireLibrary/utils/pipes/number-round.module';
import {PluginsService} from '../openaireLibrary/services/plugins.service';
import {PluginWrapperModule} from "../openaireLibrary/dashboard/plugins/wrapper/plugin-wrapper.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule,
    SubscribeModule, StatisticsModule, ManageModule, InviteModule,
    ZenodoCommunitiesServiceModule, SearchFormModule, SearchDataprovidersServiceModule, SearchProjectsServiceModule,
    SearchResearchResultsServiceModule, SearchResultsModule, CuratorsModule, AffiliationsModule,
    SEOServiceModule, MatSelectModule, EntitiesSelectionModule,
    TabsModule, SearchTabModule, ErrorMessagesModule, SafeHtmlPipeModule, ErrorModule,
    AdvancedSearchInputModule, InputModule, QuickSelectionsModule, IconsModule, NoLoadPaging, NumberRoundModule, PluginWrapperModule, LoadingModule
  ],
  declarations: [
    CommunityComponent
  ],
  providers:[
    PreviousRouteRecorder, PiwikService, PluginsService
  ],
  exports: [
    CommunityComponent
  ]
})
export class CommunityModule { 
}
