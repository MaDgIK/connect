import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonBeta} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {

  dashboard: 'connect',
  adminToolsPortalType: "connect",
  adminToolsCommunity: 'connect',
  enablePiwikTrack: true,
  piwikSiteId: '80',
  useCache: false,
  useLongCache: true,
  showContent: true,
  showAddThis: false,
  domain: 'https://beta.connect.openaire.eu',
  baseLink : "",
  monitorStatsFrameUrl:"https://beta.services.openaire.eu/stats-tool/"


};

export let properties: EnvProperties = {
  ...common, ...commonBeta, ...props
}
