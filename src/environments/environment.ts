// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular.json`.

import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
  dashboard: 'connect',
  adminToolsPortalType: "connect",
  adminToolsCommunity: 'connect',
  enablePiwikTrack: false,
  piwikSiteId: '80',
  useCache: false,
  useLongCache: false,
  showContent: true,
  showAddThis: false,
  domain: 'https://beta.connect.openaire.eu',
  baseLink : "",

  adminToolsAPIURL: 'http://scoobydoo.di.uoa.gr:8880/uoa-admin-tools/',
  utilsService:"http://scoobydoo.di.uoa.gr:8000",
  monitorStatsFrameUrl:"https://stats.madgik.di.uoa.gr/stats-api/",

};


export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}
