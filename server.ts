import 'zone.js/node';

import {ngExpressEngine} from '@nguniversal/express-engine';
import * as express from 'express';
import * as compression from 'compression';
import {join} from 'path';

import {AppServerModule} from './src/main.server';
import {APP_BASE_HREF} from '@angular/common';
import {existsSync} from 'fs';
import {REQUEST, RESPONSE} from "./src/app/openaireLibrary/utils/tokens";
import {isArray} from "util";
import {properties} from "./src/environments/environment";
import {CustomizationOptions, Layout} from "./src/app/openaireLibrary/connect/community/CustomizationOptions";
import {Response} from "express";

const fs = require('fs');
const less = require('less/index');
const minify = require('@node-minify/core');
const cleanCSS = require('@node-minify/clean-css');
const axios = require('axios');

const browser = process.cwd() + '/dist/connect/browser/';
const node_modules = process.cwd() + '/node_modules';
var bodyParser = require('body-parser')

var jsonParser = bodyParser.json()

function buildCss(portal: string, suffix = null, variables: {} = null, customCss = "") {
  let lessFile = 'community.less'
  if (portal === 'connect') {
    lessFile = 'connect.less'
  }
  let input = fs.readFileSync(browser + '/assets/' + lessFile, 'utf8');
  /* Change fonts path */
  let modifyVars = {
    '@font-media-url': 'e("assets/openaire-theme/media/fonts/aileron/")',
    '@font-media-icon-url': 'e("assets/openaire-theme/media/fonts/material-icons/")'
  }
  /* Change variables (optional)*/
  if (variables) {
    Object.entries(variables).forEach(([key, value]) => {
      modifyVars[key] = value;
    });
  }
  let options = {
    paths: [browser + '/assets/', node_modules],
    rewriteUrls: 'all',
    modifyVars: modifyVars
  };
  less.render(customCss + input , options, function (error, result) {
    if (error) {
      console.log(error);
    } else {
      let file = browser + portal + (suffix ? ("-" + suffix) : "") + '.css';
      fs.writeFile(file, result.css, function (error) {
        if (error) {
          console.error(error);
        } else {
          minify({
            compressor: cleanCSS,
            replaceInPlace: true,
            input: file,
            output: file,
            callback: function (err) {
              if (err) {
                console.log(err);
              }
            }
          });
        }
      });
    }
  });
}

function buildAll(res: Response = null) {
  let layoutsURL = properties.adminToolsAPIURL + '/community/layouts';
  axios.get(layoutsURL).then(response => {
    if (response.data && Array.isArray(response.data) && response.data.length > 0) {
      response.data.forEach((layout: Layout) => {
        let variables = Layout.getVariables(CustomizationOptions.checkForObsoleteVersion(layout.layoutOptions, layout.portalPid));
        buildCss(layout.portalPid, layout.date ? layout.date : null, variables, layout.layoutOptions.identity.customCss);
      });
      if (res) {
        res.status(200).send({
          code: 200,
          message: 'CSS build for all available layouts was successful'
        });
      }
    } else {
      res.status(500).send({code: 500, message: 'No available layouts found'});
    }
  });
}
if(properties.environment == 'development') {
  buildAll();
}

// The Express app is exported so that it can be used by serverless Functions.
export function app() {
  const server = express();
  server.use(compression());
  const distFolder = join(process.cwd(), 'dist/connect/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';
  
  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
    inlineCriticalCss: false
  }));
  
  server.set('view engine', 'html');
  server.set('views', distFolder);
  
  server.use('/build-css', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, DELETE');
    res.header('Access-Control-Max-Age', "1800");
    next();
  });
  
  server.use(function (req, res, next) {
    var XFRAME_WHITELIST = ['http://spitoo.di.uoa.gr:5000/', 'http://scoobydoo.di.uoa.gr:5000/', 'https://beta.admin.connect.openaire.eu/', 'https://admin.connect.openaire.eu/'];
    let referer: string;
    if (req.headers.referer) {
      referer = isArray(req.headers.referer) ? req.headers.referer[0] : (<string>req.headers.referer);
      referer = referer.split("?")[0];
    }
    if (referer && (XFRAME_WHITELIST.indexOf(referer) != -1 || referer.indexOf("/customize-layout") != -1 || referer.indexOf(".d4science.org") != -1 || referer.indexOf(".di.uoa.gr") != -1 || referer.indexOf(".openaire.eu") != -1 )) {
      res.header('X-FRAME-OPTIONS', 'allow from ' +req.headers.referer);
      res.header('Access-Control-Allow-Origin',req.headers.origin);
      res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Cache-control, Expires, Content-Type, Pragma');
      res.header('Allow', 'GET, HEAD, OPTIONS');

    } else {
      res.header('X-FRAME-OPTIONS', 'SAMEORIGIN');
    }
    next();
  });
  
  // - Serve sitemap based on the host -
  server.get('/sitemap.xml', (req, res) => {
    let host = req.get("host").split(":")[0];
    let connectLinks = ["/", "/about/learn-how", "/about/learn-in-depth", "/about/faq", "/search/find/communities"];
    let communityLinks = ["/", "/search/find/research-outcomes?size=20", "/search/advanced/research-outcomes?size=20", "/participate/deposit/learn-how", "/organizations", "/content"];
    let sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">";
    let urlPre = "<url>\n" +
      "    <loc>";
    let urlSuf = "</loc>\n" +
      "    </url>";
    for (let link of (host.indexOf("connect.openaire.eu") == -1 ? communityLinks : connectLinks)) {
      sitemap += urlPre + "https://" + host + link + urlSuf;
    }
    sitemap += "\n</urlset>";
    res.setHeader('content-type', 'application/xml');
    res.send(sitemap);
  });
  
  // - Serve robots based on the host -
  server.get('/robots.txt', (req, res) => {
    let host = req.get("host").split(":")[0];
    let robots = "";
    if (host.indexOf(".openaire.eu") != -1 && host.indexOf("beta.") == -1) {
      //link to disallow
      let connectLinks = ["/about/*", "/search/find/communities"];
      let communityLinks = ["/search/advanced/*", "/participate/*",
        "/search/project", '/search/result', '/search/publication', '/search/dataset', '/search/software', '/search/other', '/search/dataprovider', '/search/organization', '/project-report',
        '/search/find/publications', '/search/find/datasets', '/search/find/software', '/search/find/other', '/search/find/projects', '/search/find/dataproviders', '/search/find/research-outcomes'
      ];
      robots = "User-Agent: *\n" +
        "Crawl-delay: 30\n" +
        "Sitemap: /sitemap.xml\n";
      for (let link of (host.indexOf("connect.openaire.eu") == -1 ? connectLinks : communityLinks)) {
        robots += "Disallow: " + link + "\n";
      }
    } else {
      robots = "User-Agent: *\n" +
        "Disallow: /\n"
    }
    res.setHeader('content-type', 'text/plain');
    res.send(robots);
  });
  
  server.post('/build-css/all', (req, res) => {
    buildAll(res);
  });
  
  server.post('/build-css/:id/:suffix',  jsonParser,(req, res) => {
    let variables = Layout.getVariables(CustomizationOptions.checkForObsoleteVersion(req.body, req.params.id));
          buildCss(req.params.id , req.params.suffix, variables, req.body.layoutOptions?.identity?.customCss);
        res.status(200).send({
          code: 200,
          message: 'CSS build for ' + req.params.id  + ' layout was successful'
        });
  });
  
  server.post('/build-css/preview/:id/:suffix', jsonParser, (req, res) => {
    let variables = Layout.getVariables(CustomizationOptions.checkForObsoleteVersion(req.body, req.params.id));
      buildCss(req.params.id, req.params.suffix, variables, req.body.identity.customCss);
      res.status(200).send({code: 200, message: 'CSS build for ' + req.params.id + ' layout was successful'});
  });
  server.get('/health-check', async (_req, res, _next) => {
    var uptime = process.uptime();
    const date = new Date(uptime*1000);
    const days = date.getUTCDate() - 1,
      hours = date.getUTCHours(),
      minutes = date.getUTCMinutes(),
      seconds = date.getUTCSeconds(),
      milliseconds = date.getUTCMilliseconds();


    const healthcheck = {
      uptime: days + " days, " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds, " + milliseconds + " milliseconds",
      message: 'OK',
      timestamp: new Date()
    };
    try {
      res.send(healthcheck);
    } catch (error) {
      healthcheck.message = error;
      res.status(503).send();
    }
  });


  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));
  
  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, {
        req, providers: [
          {
            provide: APP_BASE_HREF,
            useValue: req.baseUrl
          },
          {
            provide: REQUEST, useValue: (req)
          },
          {
            provide: RESPONSE, useValue: (res)
          }
        ]
      }
    );
  });
  
  return server;
}

function run() {
  const port = process.env.PORT || 4000;
  
  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
